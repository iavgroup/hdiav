/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Jorge Freire
 * Created: 16/02/2018
 * Creación Tablas
 */


create table   if not exists HD_TUsuario
(
id int not null AUTO_INCREMENT,
usuario varchar(50),
clave varchar(10),
nombre varchar(50),
apellido varchar(50),
correo varchar(20),
activo int,
fecha_creacion TIMESTAMP,
empresa varchar(20),
idrol int not null,
PRIMARY KEY (id) 
);
--insert into HD_TUsuario(usuario,clave,nombre,apellido,correo,activo,fecha_creacion,empresa,idrol)
--values('admin','admin','Administrador','Sistemas','jfreire@iav.com.ec',1,NOW(),'GPIAV',1)
select * from HD_TUsuario;
create table   if not exists HD_TEmpresa
(
id int not null AUTO_INCREMENT,
codigo varchar(5),
empresa varchar(300),
activo int,
fecha_creacion TIMESTAMP,
PRIMARY KEY (id) 
);
SELECT * FROM HD_TEmpresa;
-- insert into HD_TEmpresa(codigo,empresa,activo,fecha_creacion)
-- values('GPIAV','IMPORTADORA ALVARADO VASCONEZ LTDA',1,NOW())

create table   if not exists HD_TPermiso
(
id int not null AUTO_INCREMENT,
ventana varchar(20),
activo int,
PRIMARY KEY (id) 
);
-- insert into HD_TPermiso(ventana, activo)
-- values('CREAR TICKET',1)
SELECT * FROM HD_TPermiso;

create table   if not exists HD_TRol
(
id int not null AUTO_INCREMENT,
rol varchar(50),
activo int,
empresaid int,
PRIMARY KEY (id) 
);
 --insert into HD_TRol(rol,activo,empresaid)
 --values('Administrador',1,1)
SELECT * FROM HD_TRol;
create table   if not exists HD_TRolPermiso
(
id int not null AUTO_INCREMENT,
rolid int,
permisoid int,
PRIMARY KEY (id) 
);
-- insert into HD_TRolPermiso(rolid,permisoid)
-- values(1,1)
select * from HD_TRolPermiso;

create table   if not exists HD_TPortafolio
(
id int not null AUTO_INCREMENT,
portafolio varchar(50),
nivel int,
padre int,
activo int,
PRIMARY KEY (id) 
);

SELECT * FROM HD_TPortafolio;

 --insert into HD_TPortafolio(portafolio,nivel,padre,activo)
 --values('Sistemas',1,0,1)

 --insert into HD_TPortafolio(portafolio,nivel,padre,activo)
 --values('Aplicaciones',2,1,1)

 --insert into HD_TPortafolio(portafolio,nivel,padre,activo)
 --values('GP',3,2,1)


create table   if not exists HD_TPortafolioRol
(
id int not null AUTO_INCREMENT,
portafolioid int,
categoriaid int,
PRIMARY KEY (id) 
);

insert into HD_TPortafolioRol(portafolioid,categoriaid)
values(1,1);

select * from HD_TPortafolioRol

/* CONSULTAS */
select * from HD_TUsuario;
SELECT * FROM HD_TEmpresa;
SELECT * FROM HD_TPermiso;
SELECT * FROM HD_TRol;
select * from HD_TRolPermiso;
SELECT * FROM HD_TPortafolio;
select * from HD_TPortafolioRol;








