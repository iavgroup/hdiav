/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  gabriel.reyes
 * Created: 16/02/2018
 */

CREATE  TABLE IF NOT EXISTS dti_website_template 
(
  id INT NOT NULL AUTO_INCREMENT,
  template VARCHAR(100) NOT NULL,
  file_name VARCHAR(100) NOT NULL,
  css_name VARCHAR(100) NOT NULL,
  image VARCHAR(200) NULL,
  PRIMARY KEY (id) 
)ENGINE = InnoDB;

INSERT INTO dti_website_template(template,file_name,css_name,image)
VALUES('template1','resources/template/template1/template.php','resources/template/template1/style.css',null);
INSERT INTO dti_website_template(template,file_name,css_name,image)
VALUES('template2','resources/template/template2/template.php','resources/template/template2/style.css',null);
INSERT INTO dti_website_template(template,file_name,css_name,image)
VALUES('template3','resources/template/template3/template.php','resources/template/template3/style.css',null);
INSERT INTO dti_website_template(template,file_name,css_name,image)
VALUES('template4','resources/template/template4/template.php','resources/template/template4/style.css',null);
INSERT INTO dti_website_template(template,file_name,css_name,image)
VALUES('template5','resources/template/template5/template.php','resources/template/template5/style.css',null);
INSERT INTO dti_website_template(template,file_name,css_name,image)
VALUES('template6','resources/template/template6/template.php','resources/template/template6/style.css',null);

/* ----------------------------------------
        Tabla Website Language
---------------------------------------- */
CREATE  TABLE IF NOT EXISTS dti_website_language
(
  id INT NOT NULL AUTO_INCREMENT,
  lenguaje VARCHAR(150) NOT NULL ,
  abreviatura VARCHAR(15) NOT NULL ,
  descripcion TEXT NULL ,
  PRIMARY KEY (id) 
)ENGINE = InnoDB;

INSERT INTO dti_website_language(lenguaje,abreviatura,descripcion)
VALUES('ESPAÑOL','es','Español Latinomareicano');

/* ----------------------------------------
        Tabla Website
---------------------------------------- */
CREATE TABLE IF NOT EXISTS dti_website
(
    id INT NOT NULL AUTO_INCREMENT,
    languageid INT NOT NULL ,
    template_core INT NOT NULL COMMENT 'website template' ,
    template_portal INT NOT NULL COMMENT 'website template' ,
    nombre VARCHAR(200) NULL COMMENT '\n' ,
    description TEXT NULL ,
    telefono VARCHAR(45) NULL,
    keywords TEXT NULL ,
    website_url VARCHAR(45) NULL ,
    logo VARCHAR(200) NULL COMMENT 'general logo' ,
    icon VARCHAR(200) NULL COMMENT 'general icon' ,
    info_email VARCHAR(45) NULL ,
    copyright TEXT NULL ,
    smtp_hostname VARCHAR(200) NULL ,
    smtp_port INT NULL ,
    smtp_username VARCHAR(100) NULL ,
    smtp_password VARCHAR(100) NULL ,
    iva INT,
    clase_clientes VARCHAR(100),
    wsclientes VARCHAR(100),
    wssucursales VARCHAR(100),
    wscobranza VARCHAR(100),
    wspedido VARCHAR(100),
    wspedidodetalle VARCHAR(100),
    chequera VARCHAR(100),
    PRIMARY KEY (id),
    CONSTRAINT fk_dti_website_template FOREIGN KEY (template_core)
    REFERENCES dti_website_template (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_dti_website_templ_por FOREIGN KEY (template_portal)
    REFERENCES dti_website_template (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_dti_website_language1 FOREIGN KEY (languageid)
    REFERENCES dti_website_language (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE = InnoDB;

INSERT INTO dti_website(languageid,template_core,template_portal,nombre,description,telefono,keywords,website_url,logo,icon,info_email,copyright,smtp_hostname,smtp_port,smtp_username,smtp_password,iva)
VALUES(1,1,5,'HD','HD','0995466833','Baldosas, pisos','localhost:81/HD/',null,null,'ventas@iav.com','Dtiware','smtp.gmail.com',0,'','',12);

--update dti_website set template_portal=5 where id =1

