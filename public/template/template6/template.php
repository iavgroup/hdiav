<main>
    <div class="dti_contact">
    </div>
    <div class="dti_header">
        <div class="dti_logo">
            <img src="public/images/logo.jpg" alt="">
            <h2>DtiWare</h2>
        </div>
        <div class="dti_titulo"></div>
        <div class="dti_submenu">
            <ul class="nav navbar-nav navbar-right dti_menu">
                <li><a href="template/index"><span class="fa fa-home"></span></a></li>
                <li><a href="template/login"><span class="fa fa-user"></span></a></li>
            </ul>
        </div>
    </div><!--Fin Header-->
    <div class="dti_contenido">
        <div class="row" id="dti_area_content">
        </div>
    </div><!--Fin Contenido-->
    <div class="dti_footer">
    </div><!--Fin Footer-->
</main>