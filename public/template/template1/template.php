<!-- **HEADER** -->
<div class="container-fluid">
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-4">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span id="dti_logo"></span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-4">
          <ul class="nav navbar-nav navbar-right" id="dti_menu">
          </ul>
        </div>
      </div>
    </nav>
</div>
<!-- **FIN HEADER ** -->
<!-- **CONTENIDO** -->
<div class="container">
    <div class="row" id="dti_area_content">
    </div>
</div>
<!-- **FIN CONTENIDO ** -->
<!-- **FOOTER** -->
<footer>
    <!--<div class="footer" id="footer">
    </div>
    <!--/.footer-->
    <div class="footer-bottom" id="dti_footer">
    </div>
    <!--/.footer-bottom-->
</footer>
<!-- **FIN FOOTER ** -->