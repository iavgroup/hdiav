<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_core {
    
    protected static $data;
    
    public function __construct($tipo) {
        switch ($tipo) {
            case "template1":
                self::set("css","<link href='public/template/template1/style.css' rel='stylesheet' type='text/css'/>");
                //self::set("script","<script src='resources/template/template1/template.js' type='text/javascript'></script>");
                break;
            case "template2":
                self::set("css","<link href='public/template/template2/style.css' rel='stylesheet' type='text/css'/>");
                break;
            case "template3":
                self::set("css","<link href='public/template/template3/style.css' rel='stylesheet' type='text/css'/>");
                self::set("script","<script src='public/template/template3/template.js' type='text/javascript'></script>");
                break;
            case "template4":
                self::set("css","<link href='public/template/template4/style.css' rel='stylesheet' type='text/css'/>");
                self::set("script","<script src='public/template/template4/template.js' type='text/javascript'></script>");
                break;
            case "template5":
                self::set("css","<link href='public/template/template5/style.css' rel='stylesheet' type='text/css'/>");
                self::set("script","<script src='public/template/template5/template.js' type='text/javascript'></script>");
                break;
        }
    }

    public static function set($name, $value){
        if (isset(self::$data[$name])) {
            self::$data[$name] .= $value;
        }else{
            self::$data[$name] = $value;
        }
    }

    public static function get(){
        //Activar o desactivar Parametros especiales
        return self::$data;
    }
}