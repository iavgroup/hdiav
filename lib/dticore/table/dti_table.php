<?php

/*
 * Titulo: Creador de Tablas.
 * Author: Gabriel Reyes
 * Fecha: 10/05/2017
 * Version: 1.0.1
 *    */

class dti_table {
    
    //Variables Principales
    private static $primarykey,$idtable,$filterpaginacion,$paginacion,$titulo,$subtitulo,$nuevo,$editar,$eliminar,$columnas,$etiquetas,$datos,$table,$filtro,$codfiltro,$ctlVariables;
    //Variables Auxiliares
    private static $modaledit,$editsubclave,$btnAccion,$btnlink,$btnSession,$tipoColumn;
    
    static function setModalEdit($modaledit){
        self::$modaledit = $modaledit;
    }
    
    static function getModalEdit() {
        return self::$modaledit;
    }
    
    static function setBtnAccion($btnAccion,$btnlink='',$btnSession=''){
        self::$btnAccion = $btnAccion;
        self::$btnlink = $btnlink;
        self::$btnSession = $btnSession;
    }
    
    static function getBtnAccion() {
        return self::$btnAccion;
    }
    
    static function getTitulo() {
        return self::$titulo;
    }

    static function getSubtitulo() {
        return self::$subtitulo;
    }

    static function getNuevo() {
        return self::$nuevo;
    }

    static function getEditar() {
        return self::$editar;
    }

    static function getEliminar() {
        return self::$eliminar;
    }

    static function getColumnas() {
        return self::$columnas;
    }
    
    static function getTipoColumn() {
        return self::$tipoColumn;
    }

    static function getEtiquetas() {
        return self::$etiquetas;
    }

    static function getDatos() {
        return self::$datos;
    }

    static function getFiltro() {
        return self::$filtro;
    }

    static function getIdtable() {
        return self::$idtable;
    }
    
    static function getPaginacion() {
        return self::$paginacion;
    }
    
    static function getFilterpaginacion() {
        return self::$filterpaginacion;
    }
    
    private static function setFilterpaginacion($filterpaginacion,$funcion) {
        if ($filterpaginacion) {
            self::$filtro = "<div class='pull-right btn btn-primary btn-sm'><span class='clickable filter' data-toggle='tooltip' title='Toggle table filter' data-container='body'><i class='fa fa-search'></i> Busqueda</span></div>";
            self::$codfiltro = "<br>
                                <div class='col-lg-12 col-md-12 filtros'>
                                    <div class='input-group'>
                                        <input class='form-control' id='system-search' id='q' name='q' placeholder='Texto a Buscar' required onkeyup='$funcion(1)'>
                                        <span class='input-group-btn'>
                                            <button class='btn btn-info' type='button'>
                                                <i class='fa fa-search'></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>";
            $variables = new \dti_core("table_filtro");
        }else{
            self::$filtro = "";
            self::$codfiltro = "";
        }
    }
    
    static function setFiltro($filtro,$funcion="",$accion="",$id="") {
        if ($filtro) {
            if (strlen($funcion)>2) {
                $html = "<input class='form-control' id='q".self::$idtable."' name='q".self::$idtable."' placeholder='Texto a Buscar' required onkeyup='$funcion(1)'>";
            }else{
                $html = "<input class='form-control' id='system-search' name='q' placeholder='Texto a Buscar' required>";
            }
            
            self::$filtro = "<div class='pull-right btn btn-primary btn-sm'><span class='clickable filter' data-toggle='tooltip' title='Toggle table filter' data-container='body'><i class='fa fa-search'></i> Buscar</span></div>";
            
            self::$codfiltro = "<br>
                                <div class='col-lg-12 col-md-12 filtros'>
                                    <div class='input-group'>
                                        ".$html."
                                        <span class='input-group-btn'>
                                            <button class='btn btn-info' type='button'>
                                                <i class='fa fa-search'></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>";
            $variables = new \dti_core("table_filtro");
            
            if (strlen($id)) $data = "data: 'page='+page+'&q='+q+'&tipo=tabla'+'&w=".$id."',";
            else $data = "data: 'page='+page+'&q='+q+'&tipo=tabla',";
            
            //Creamos el JavaScript
            dti_core::set("script", "<script>".$funcion."(1);
                                    function ".$funcion."(page){
                                        //Cojemos la variable.
                                        var q= $('#q".self::$idtable."').val();
                                        $('#loader".self::$idtable."').fadeIn('slow');
                                        $.ajax({
                                                //Escogemos la URL donde vamos a buscar.
                                                url:'".CONTROLADOR_DEFECTO."/".$accion."/',
                                                //Envialos los parametros.
                                                ".$data."
                                                //Escogemos el metodo de envio en esta caso POST.
                                                type: 'post',
                                                //Mostramos una imagen y la palabra cargando mientras espera.
                                                beforeSend: function(){
                                                    $('#loader".self::$idtable."').html('<img src=\"public/images/ajax-loader.gif\"> Cargando...');
                                                },
                                                //Una vez que termino mostramos los datos y limpiamos el cargando.
                                                success:function(data){
                                                    $('.outer_div".self::$idtable."').html(data).fadeIn('slow');
                                                    $('#loader".self::$idtable."').html('');
                                                }
                                            });
                                    }</script>");
        }else{
            self::$filtro = "";
            self::$codfiltro = "";
        }
    }
    
    static function setIdtable($idtable) {
        self::$idtable = $idtable;
    }
    
    private static function setPaginacion($reload,$page,$total_pages,$adjacents,$function) {
        self::$paginacion = \config\globalFunctions::paginate($reload, $page, $total_pages, $adjacents,$function);
    }
    
    static function setTitulo($titulo) {
        self::$titulo = "<h4 class='title'>".$titulo."</h4>";
    }

    static function setSubtitulo($subtitulo) {
        self::$subtitulo = "<p class='category'>$subtitulo</p>";
    }
    
    /*
     * #################################################
     *      SOPORTE PARA Nueva Ventana o Modal
     * #################################################
     * Autor: Gabriel Reyes
     * Fecha: 15/09/2017
     * Version: 1.0.1
     */
    static function setNuevo($url,$modal=false,$nomModal='') {
        if ($modal) {
            self::$nuevo = '<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#mdl'.$nomModal.'">Nuevo</button>';
            $modal = "<!-- Modal -->
                                <div class='modal fade bs-example-modal-lg' id='mdl".$nomModal."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                                  <div class='modal-dialog modal-lg' role='document'>
                                        <div class='modal-content'>
                                          <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                                <h4 class='modal-title' id='myModalLabel'> Nuevo Registro</h4>
                                          </div>
                                          <div class='modal-body'>
                                                ".$url."
                                          </div>
                                          <div class='modal-footer'>
                                                <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                                          </div>
                                        </div>
                                  </div>
                                </div>";
            dti_core::set("modal", $modal);
        }
        else{
            self::$nuevo = "<a class='btn btn-primary btn-sm pull-right' href='".$url."'><span class='fa fa-edit'></span> Nuevo</a>";
        }
    }

    static function setEditar($url,$modal=false,$nomModal='') {
        if ($modal) {
            self::$modaledit = true;
            //self::$editar = '<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mdl'.$nomModal.'"><span class="fa fa-edit"></span>Editar</button>';
            self::$editar = $nomModal;
            $modal = "<!-- Modal -->
                                <div class='modal fade bs-example-modal-lg' id='mdl".$nomModal."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                                  <div class='modal-dialog modal-lg' role='document'>
                                        <div class='modal-content'>
                                          <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                                <h4 class='modal-title' id='myModalLabel'> Editar Registro</h4>
                                          </div>
                                          <div class='modal-body'>
                                            <div id='loader".$nomModal."' style='position: absolute;text-align: center;top: 55px;width: 100%;display:none;'></div>
                                            <div class='outer_div".$nomModal."'></div>
                                          </div>
                                          <div class='modal-footer'>
                                                <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                                          </div>
                                        </div>
                                  </div>
                                </div>";
            dti_core::set("modal", $modal);
            $script = "<script>
                        $(function() {
                            $(document).on('click','.mdl".$nomModal."',function(e){
                                $('#loader".$nomModal."').fadeIn('slow');
                                 $.ajax({
                                        //Escogemos la URL donde vamos a buscar.
                                        url:'".$url."',
                                        //Envialos los parametros.
                                        data: 'edit='+$(this).attr('id-edit')+'&panel=true',
                                        //Escogemos el metodo de envio en esta caso POST.
                                        type: 'post',
                                        //Mostramos una imagen y la palabra cargando mientras espera.
                                        beforeSend: function(objeto){
                                            $('#loader".$nomModal."').html(\"<img src='public/images/ajax-loader.gif'> Cargando...\");
                                        },
                                        //Una vez que termino mostramos los datos y limpiamos el cargando.
                                        success:function(data){
                                            $('.outer_div".$nomModal."').html(data).fadeIn('slow');
                                            $('#loader".$nomModal."').html('');
                                            $('.fecha_edit').datetimepicker({
                                                language:  'es',
                                                weekStart: 1,
                                                todayBtn:  1,
                                                autoclose: 1,
                                                todayHighlight: 1,
                                                startView: 2,
                                                minView: 2,
                                                forceParse: 0,
                                                format: 'yyyy-mm-dd'
                                            });
                                        }
                                    });
                             });
                        });
                        </script> ";
            dti_core::set("script", $script);
        }
        else{
            self::$modaledit = false;
            self::$editar = $url;
        }
    }

    static function setEliminar($url,$editsubclave='') {
        self::$eliminar = $url;
        self::$editsubclave = $editsubclave;
    }

    static function setColumnas($columnas) {
        self::$columnas = $columnas;
    }
    
    static function setTipoColumn($tipoColumn) {
        self::$tipoColumn = $tipoColumn;
    }

    static function setEtiquetas($etiquetas) {
        self::$etiquetas = $etiquetas;
    }

    static function setDatos($datos) {
        self::$datos = $datos;
    }

    public function __construct($primarykey='') {
        self::$primarykey = $primarykey;
        $this->setFiltro(false);
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("table");
            self::$ctlVariables = 0;
        }
        self::$modaledit = false;
    }

    public function setTable($datos){
        self::$table = $datos;
    }

    /**
     * Funcion getTable
     * 
     * El $tipo puede ir en blanco cuando solo necesites usar otra versión.
     * Para Crear Campo descarga enviar con *
     * Para Crear Campo txt enviar con +
     * 
     * @author Gabriel Reyes
     * @version 2.0.1
     * 
     * @param string $tipo Soportados PAGINACION / DPAGINACION
     * @param int $version 1-> Paginacion y Filtros 2->Para busquedas desde Txt
     */
    public function gettable($tipo='',$version=1,$id=''){
        $es_arrar  = false;
        $columnas = "";
        $filas = "";
        $numfilas = 0;
        $numcolumnas = 0;
        //Llena todas las columnas
        foreach (explode(",", $this->getEtiquetas()) as $col) {
            $columnas .= "<th>".$col."</th>";
            $numcolumnas++;
        }
        if ($this->getEditar() || $this->getEliminar() || $this->getBtnAccion() ) {
            $columnas .= "<th> Acciones </th>";
        }
        if (is_array($this->getDatos())) {
            foreach ($this->getDatos() as $col) {
                $numfilas++;
            }

            if ($numfilas > 1) {

                foreach ($this->getDatos() as $value) {
                    if (is_array($value)) {
                        $es_arrar = TRUE;
                    }
                }
                
                if ($es_arrar) {
                    foreach ($this->getDatos() as $value) {
                        $filas .= "<tr>";
                        foreach (explode(",", $this->getColumnas()) as $col) {
                            $pos = strpos($col, '*');
                            if ($pos === false) {
                                $pos = strpos($col, '+');
                                if ($pos === false) {
                                    if ($col == 'id') {
                                        $filas .= "<td class='".$this->getIdtable()."'>".$value[$col]."</td>";
                                    }else{
                                        $filas .= "<td>".$value[$col]."</td>";
                                    }
                                }else{
                                    $filas .= "<td><input id='txt".substr($col, 0, -1)."".$value["producto"]."' name='txt".substr($col, 0, -1)."".$value["producto"]."' type='number' value='".$value[substr($col, 0, -1)]."' class='form-control' style='width:100px;'/></td>";
                                }
                            }else{
                                $filas .= "<td>".$value[substr($col, 0, -1)]."</td>";
                            }
                        }
                        //$filas .= "<td class='text-center'>";
                        $filas .= "<td>";
                        if ($this->getEditar()) {
                            if (self::$modaledit) {
                                $filas .= '<button id-edit="'.$value["id"].'" class="btn btn-primary btn-xs mdl'.$this->getEditar().'" data-toggle="modal" data-target="#mdl'.$this->getEditar().'"><span class="fa fa-edit"></span>Editar</button>';
                            }else{
                                //Version 1
                                //$filas .= " <a class='btn btn-info btn-xs' href='".$this->getEditar()."/".$value["id"]."'><span class='fa fa-edit'></span> Editar</a> ";
                                //Version 2
                                $filas .= " <a data-toggle='tooltip' title='Editar' class='btn btn-info btn-xs' href='".$this->getEditar()."/".$value["id"]."'><span class='fa fa-edit'></span></a> ";
                            }
                        }
                        if ($this->getEliminar()) {
                            switch (ELIMINAR_JS) {
                                case 0:
                                        if (strlen(self::$editsubclave)>0) {
                                            if (isset($value["id"])) {
                                                $filas .= " <a href='".$this->getEliminar()."/".$value["id"]."/".$value[self::$editsubclave]."' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span></a> ";
                                            }else{
                                                $filas .= " <a href='".$this->getEliminar()."/".$value[self::$editsubclave]."' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span></a> ";
                                            }
                                        }else{
                                            $filas .= " <a href='".$this->getEliminar()."/".$value["id"]."' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span></a> ";
                                        }
                                    break;
                                case 1:
                                    $filas .= " <a data-toggle='tooltip' title='Eliminar' class='delete_mies btn btn-danger btn-xs' data-link-id='".$this->getEliminar()."' data-pk-id='".$value["id"]."' href='javascript:void(0)'><i class='fa fa-remove'></i></a>";
                                    break;
                            }
                        }
                        if ($this->getBtnAccion()) {
                            switch ($this->getBtnAccion()) {
                                case 'LINK':
                                    if (isset($value["codigo"])) {
                                        if (strlen($id)>0) $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$id.'/'.$value["codigo"].'"><i class="fa fa-check"></i></a>';
                                        else $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$value["codigo"].'"><i class="fa fa-check"></i></a>';
                                    }else{
                                        if (strlen($id)>0) $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$id.'/'.$value["id"].'"><i class="fa fa-check"></i></a>';
                                        else $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$value["id"].'"><i class="fa fa-check"></i></a>';
                                    }
                                    break;
                                case 'SESSION':
                                    //Creamos la Variable de Session que necesitemos.
                                    if (strlen(self::$btnSession) > 0 && strlen(self::$primarykey)>0) $valor = $value[self::$primarykey];
                                    else if (strlen(self::$btnSession) > 0) $valor = $value["id"];
                                    $filas .= '<button class="btn btn-primary btn-small" onclick="goAsignaSession(\''.self::$btnSession.'\',\''.$valor.'\')"><i class="fa fa-check"></i></button>';
                                    break;
                                case 'UPDATE':
                                    $filas .= '<a class="btn btn-primary btn-xs" onclick="'.self::$btnlink.'(\''.$value["producto"].'\')"><i class="fa fa-check"></i></a>';
                                    break;
                                case 'STEP':
                                    $filas .= $this->getBtnAccion();
                                    break;
                            }
                        }
                        $filas .= "</td></tr>";
                    }
                }else{
                    $filas .= "<tr>";
                    foreach (explode(",", $this->getColumnas()) as $col) {
                        $pos = strpos($col, '*');
                        if ($pos === false) {
                            $pos = strpos($col, '+');
                            if ($pos === false) {
                                $filas .= "<td>".$this->getDatos()[$col]."</td>";
                            }else{
                                $filas .= "<td><input id='txt".substr($col, 0, -1)."".$this->getDatos()["producto"]."' name='txt".substr($col, 0, -1)."".$this->getDatos()["producto"]."' type='number' value='".$this->getDatos()[substr($col, 0, -1)]."' class='form-control' style='width:100px;'/></td>";
                            }
                        }else{
                            $filas .= "<td><a href='".$this->getDatos()[substr($col, 0, -1)]."'>Descargar</a></td>";
                        }
                    }

                    $filas .= "<td>";
                    if ($this->getEditar()) {
                        if (self::$modaledit) {
                            $filas .= '<button id-edit="'.$this->getDatos()["id"].'" class="btn btn-primary btn-xs mdl'.$this->getEditar().'" data-toggle="modal" data-target="#mdl'.$this->getEditar().'"><span class="fa fa-edit"></span>Editar</button>';
                        }else{
                            //Version 1
                            //$filas .= " <a class='btn btn-info btn-xs' href='".$this->getEditar()."/".$this->getDatos()["id"]."'><span class='fa fa-edit'></span> Editar</a> ";
                            //Version 2
                            $filas .= " <a data-toggle='tooltip' title='Editar' class='btn btn-info btn-xs' href='".$this->getEditar()."/".$this->getDatos()["id"]."'><span class='fa fa-edit'></span></a> ";
                        }
                    }
                    if ($this->getEliminar()) {
                        switch (ELIMINAR_JS) {
                            case 0:
                                if (strlen(self::$editsubclave)>0) {
                                    if (isset($value["id"])) {
                                        $filas .= " <a href='".$this->getEliminar()."/".$this->getDatos()["id"]."/".$this->getDatos()[self::$editsubclave]."' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span></a> ";
                                    }else{
                                        $filas .= " <a href='".$this->getEliminar()."/".$this->getDatos()[self::$editsubclave]."' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span></a> ";
                                    }
                                }else{
                                    $filas .= " <a href='".$this->getEliminar()."/".$this->getDatos()["id"]."' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span></a> ";
                                }
                                break;
                            case 1:
                                $filas .= " <a data-toggle='tooltip' title='Eliminar' class='delete_mies btn btn-danger btn-xs' data-link-id='".$this->getEliminar()."' data-pk-id='".$this->getDatos()["id"]."' href='javascript:void(0)'><i class='fa fa-remove'></i></a>";
                                break;
                        }
                    }
                    if ($this->getBtnAccion()) {
                        switch ($this->getBtnAccion()) {
                            case 'LINK':
                                if (isset($value["codigo"])) {
                                    if (strlen($id)>0) $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$id.'/'.$this->getDatos()["codigo"].'"><i class="fa fa-check"></i></a>';
                                    else $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$this->getDatos()["codigo"].'"><i class="fa fa-check"></i></a>';
                                }else{
                                    if (strlen($id)>0) $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$id.'/'.$this->getDatos()["id"].'"><i class="fa fa-check"></i></a>';
                                    else $filas .= '<a class="btn btn-primary btn-small" href="'.self::$btnlink.'/'.$this->getDatos()["id"].'"><i class="fa fa-check"></i></a>';
                                }
                                break;
                            case 'SESSION':
                                //Creamos la Variable de Session que necesitemos.
                                if (strlen(self::$btnSession) > 0 && strlen(self::$primarykey)>0) $valor = $this->getDatos()[self::$primarykey];
                                else if (strlen(self::$btnSession) > 0) $valor = $this->getDatos()["id"];
                                $filas .= '<button class="btn btn-primary btn-small" onclick="goAsignaSession(\''.self::$btnSession.'\',\''.$valor.'\')"><i class="fa fa-check"></i></button>';
                                break;
                            case 'UPDATE':
                                    $filas .= '<a class="btn btn-primary btn-xs" onclick="'.self::$btnlink.'(\''.$this->getDatos()["producto"].'\')"><i class="fa fa-check"></i></a>';
                                break;
                            case 'STEP':
                                $filas .= $this->getBtnAccion();
                                break;
                        }
                    }
                    $filas .= "</td></tr>";
                }
            }
            else{
                $filas .= "<tr>";
                $filas .= "<td style='text-align:center;' colspan='".$numcolumnas."'>No existe datos</td>";
                $filas .= "<td>";
            }
        }

        switch ($version) {
            case 1:
                if ($tipo == 'paginacion') {
                    $tableAction = "<div class='col-md-12'>
                                    <div class='card card-plain'>";
                    if (strlen($this->getTitulo())>0 || strlen($this->getFiltro())>0 || strlen($this->getNuevo())>0 || strlen($this->getSubtitulo())>0) {
                        $tableAction .= "<div class='card-header' data-background-color='orange'>
                                            ".$this->getFiltro()."
                                            ".$this->getNuevo()."
                                            ".$this->getTitulo()."
                                            ".$this->getSubtitulo()."
                                        </div>";
                    }
                    $tableAction .= "".self::$codfiltro."
                                        <div class='card-content table-responsive'>";

                    $tableAction .= "<div id='loader".$this->getIdtable()."' style='position: absolute;text-align: center; top: 55px; width: 100%;display:none;'></div>
                                        <div class='outer_div".$this->getIdtable()."' ></div>";
                    $tableAction .= "</div>
                                </div>
                              </div>";

                    switch (ELIMINAR_JS) {
                        case 0:
                            //v1 no tiene JS
                            $tableAction .= "";
                            break;
                        case 1:
                            dti_core::set("js", "<script>
                                $(document).ready(function(){
                                    $('.delete_mies').click(function(e){
                                        alert('hola');
                                    });
                                });</script>");
                            break;
                    }
                }
                else{
                    //Paginacion
                    if ($tipo == 'Dpaginacion') {

                        if (strlen($filas)==0) {
                            $filas = '<tr class="search-sf"><td class="text-muted" colspan="6">No existe información.</td></tr>';
                        }

                        $tableAction = "<table class='table table-hover'>
                                            <thead>
                                                <tr>
                                                    ".$columnas."
                                                </tr>
                                            </thead>
                                            <tbody>
                                            ".$filas."
                                            </tbody>
                                        </table>";
                        if ($this->getPaginacion()) {
                            $tableAction .= '<div class="table-pagination pull-right">'.$this->getPaginacion().'</div>';
                        }
                    }
                    else{
                            $tableAction = "<div class='col-md-12'>
                                        <div class='card card-plain'>";
                            if (strlen($this->getTitulo())>0 || strlen($this->getFiltro())>0 || strlen($this->getNuevo())>0 || strlen($this->getSubtitulo())>0) {
                                $tableAction .= "<div class='card-header' data-background-color='purple'>
                                                   ".$this->getFiltro()."
                                                   ".$this->getNuevo()."
                                                   ".$this->getTitulo()."
                                                   ".$this->getSubtitulo()."
                                               </div>";
                            }
                            $tableAction .= "".self::$codfiltro."
                                            <div class='card-content table-responsive'>";

                        $tableAction .= "<table class='table table-hover'>
                                        <thead>
                                            <tr>
                                                ".$columnas."
                                            </tr>
                                        </thead>
                                        <tbody>
                                        ".$filas."
                                        </tbody>
                                    </table>";

                        $tableAction .= "</div>
                                    </div>
                                  </div>";

                        switch (ELIMINAR_JS) {
                            case 0:
                                //v1 no tiene JS
                                $tableAction .= "";
                                break;
                            case 1:
                                dti_core::set("script", "<script>
                                    $(document).ready(function(){
                                        $('.delete_".$this->getIdtable()."').click(function(e){
                                            e.preventDefault();
                                            var id = $(this).attr('data-pk-id');
                                            var urldelete = $(this).attr('data-link-id');
                                            var parent = $(this).parent(\"td\").parent(\"tr\");
                                            bootbox.dialog({
                                            message: \"Estas seguro que deseas eliminar el registo \"+id+\" ?\",
                                            title: \"<i class='glyphicon glyphicon-trash'></i> Eliminar !\",
                                            buttons: {
                                                success: {
                                                    label: \"No\",
                                                    className: \"btn-success\",
                                                    callback: function() {
                                                        $('.bootbox').modal('hide');
                                                    }
                                                },
                                                danger: {
                                                    label: \"Eliminar!\",
                                                    className: \"btn-danger\",
                                                    callback: function() {
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: urldelete,
                                                            data: 'id='+id
                                                        })
                                                        .done(function(response){
                                                            if (response == 'OK') {
                                                                bootbox.alert('Correcto, Eliminados con exito');
                                                                parent.fadeOut('slow');
                                                                location.reload();
                                                            }else{
                                                                //Agregar el COntrol de DESARROLLO al crear desde PHP
                                                                bootbox.alert('Error, No puede Eliminar o Actualizar clave Foreanea.');
                                                            }
                                                        })
                                                        .fail(function(){
                                                            bootbox.alert('Error....');
                                                        })
                                                    }
                                                }
                                            }
                                            });
                                        });
                                    });</script>");
                                break;
                        }
                    }
                }
                break;
            case 2:
                $tableAction = "<div class='col-md-12'>
                                    <div class='card card-plain'>
                                        <div class='card-header' data-background-color='orange'>
                                            ".$this->getFiltro()."
                                            ".$this->getNuevo()."
                                            ".$this->getTitulo()."
                                            ".$this->getSubtitulo()."
                                        </div>
                                        ".self::$codfiltro."
                                        <div class='card-content table-responsive'>";

                $tableAction .= "<div id='loader".$this->getIdtable()."' style='position: absolute;text-align: center; top: 55px; width: 100%;display:none;'></div>
                                    <div class='outer_div".$this->getIdtable()."' ></div>";
                $tableAction .= "</div>
                            </div>
                          </div>";
                //<div class='outer_div".$this->getIdtable()."' style='margin-top: -55px;' ></div>";
                break;
        }

        return $tableAction;
    }
}
