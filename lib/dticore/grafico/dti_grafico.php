<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_grafico {

    private static $txtX,$txtY,$titulo,$tipo,$data,$grafico,$ctlVariables;
    
    private static function getTipo() {
        return self::$tipo;
    }

    static function setTipo($tipo) {
        self::$tipo = $tipo;
    }
    
    private static function getData() {
        return self::$data;
    }

    static function setData($data) {
        self::$data = $data;
    }
    
    private static function getTxtX() {
        return self::$txtX;
    }

    private static function getTxtY() {
        return self::$txtY;
    }

    private static function getTitulo() {
        return self::$titulo;
    }

    static function setTxtX($txtX) {
        self::$txtX = $txtX;
    }

    static function setTxtY($txtY) {
        self::$txtY = $txtY;
    }

    static function setTitulo($titulo) {
        self::$titulo = $titulo;
    }

    function setGrafico(){
        self::$grafico = '<div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="box">
                                    <div class="box-icon">
                                        <span class="fa fa-4x fa-html5"></span>
                                    </div>
                                    <div class="info">
                                        <h4 class="text-center">Title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                                        <a href="" class="btn">Link</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="box">
                                    <div class="box-icon">
                                        <span class="fa fa-4x fa-css3"></span>
                                    </div>
                                    <div class="info">
                                        <h4 class="text-center">Title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                                        <a href="" class="btn">Link</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>';
    }

    public function __construct() {
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("grafico");
            self::$ctlVariables = 0;
        }
    }

    public function getGrafico(){
        switch ($this->getTipo()) {
            case 'column':
                $dtVariables = "";
                $dtSeries = "";
                foreach ($this->getData() as $serial => $valores) {
                    $dtVariables .= "var ".$serial." = ".$valores.";";
                    $dtSeries .= "{ name: '".$serial."', data: ".$serial." },";
                }
                //Eliminamos el ultimo Caracter
                $dtSeries = substr($dtSeries, 0, -1);
                //Armamos todo el grafico
                $graficoScript = "<script>$(function () {
                                            ".$dtVariables."
                                            $('#dticolumn').highcharts({
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: '".$this->getTitulo()."'
                                                },
                                                xAxis: {
                                                    categories: ".$this->getTxtX()."
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: '".$this->getTxtY()."'
                                                    }
                                                },
                                                series: [".$dtSeries."]
                                            });
                                        });</script>";
                self::$grafico = "<div id='dticolumn'></div>";
                \dti_core::set("script", $graficoScript);
                break;
            case 'line':
                $dtVariables = "";
                $dtSeries = "";
                foreach ($this->getData() as $serial => $valores) {
                    $dtVariables .= "var ".$serial." = ".$valores.";";
                    $dtSeries .= "{ name: '".$serial."', data: ".$serial." },";
                }
                //Eliminamos el ultimo Caracter
                $dtSeries = substr($dtSeries, 0, -1);
                //Armamos todo el grafico
                $graficoScript = "<script>$(function () {
                                            ".$dtVariables."
                                            $('#dtiline').highcharts({
                                                chart: {
                                                    type: 'line'
                                                },
                                                title: {
                                                    text: '".$this->getTitulo()."'
                                                },
                                                xAxis: {
                                                    categories: ".$this->getTxtX()."
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: '".$this->getTxtY()."'
                                                    }
                                                },
                                                series: [".$dtSeries."]
                                            });
                                        });</script>";
                self::$grafico = "<div id='dtiline'></div>";
                \dti_core::set("script", $graficoScript);
                break;
            case 'pie':
                $dtVariables = "";
                $dtSeries = "";
                foreach ($this->getData() as $serial => $valores) {
                    $dtVariables .= "var ".$serial." = ".$valores.";";
                    $dtSeries .= "{ name: '".$serial."', data: ".$serial." },";
                }
                //Eliminamos el ultimo Caracter
                $dtSeries = substr($dtSeries, 0, -1);
                //Armamos todo el grafico
                $graficoScript = "<script>$(function () {
                                            ".$dtVariables."
                                            $('#dtipie').highcharts({
                                                chart: {
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: '".$this->getTitulo()."'
                                                },
                                                xAxis: {
                                                    categories: ".$this->getTxtX()."
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: '".$this->getTxtY()."'
                                                    }
                                                },
                                                series: [".$dtSeries."]
                                            });
                                        });</script>";
                self::$grafico = "<div id='dtipie'></div>";
                \dti_core::set("script", $graficoScript);
                break;
        }
        return self::$grafico;
    }
}