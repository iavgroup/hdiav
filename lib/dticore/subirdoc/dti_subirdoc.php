<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_subirdoc {

    private static $subirdoc,$ctlVariables;

    function setSubirdoc($datos){
        if ($datos["panel"]) {
            //Titulo - Accion - Subtitulo
            self::$subirdoc = "<div class='panel panel-primary'>
                                <div class='panel-heading'>".$datos["titulo"]."</div>
                                <div class='panel-body'>
                                    <form action='".$datos["accion"]."' method='post' enctype='multipart/form-data' >
                                        <div class='form-group row'>
                                            <label for='subirdoc' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos["label"]."</label>
                                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                                <input type='file' id='subirdoc' name='subirdoc' class='filestyle' data-placeholder='Seleccionar Documento' data-buttonText='Seleccionar' data-buttonName='btn-primary' >
                                            </div>
                                        </div>
                                        <div class='form-group row'><div class='col-sm-offset-2 col-sm-4'>
                                            <input type='submit' value='Importar' class='btn btn-warning col-lg-12 col-md-12 col-sm-12' />
                                        </div></div>
                                    </form>
                                </div>
                            </div>";
        }else{
            self::$subirdoc = "<form action='".$datos["accion"]."' method='post' enctype='multipart/form-data' >
                                    <div class='form-group row'>
                                        <label for='subirdoc' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos["label"]."</label>
                                        <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                            <input type='file' id='subirdoc' name='subirdoc' class='filestyle' data-placeholder='Seleccionar Documento' data-buttonText='Seleccionar' data-buttonName='btn-primary' >
                                        </div>
                                    </div>
                                    <div class='form-group row'><div class='col-sm-offset-2 col-sm-4'>
                                        <input type='submit' value='Importar' class='btn btn-warning col-lg-12 col-md-12 col-sm-12' />
                                    </div></div>
                                </form>";
        }
    }

    public function __construct() {
        //Cargas Css/Js/Script Obligatorios
        //if (!isset(self::$ctlVariables)) {
            //$variables = new \dti_core("subirdoc");
            //self::$ctlVariables = 0;
        //}
    }

    public function getSubirdoc(){
        return self::$subirdoc;
    }
}
