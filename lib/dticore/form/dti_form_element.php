<?php

class dti_form_element {

    public static $btnInput,$icono,$inputImage,$inputImage2,$onClick, $selectid, $select,$errorText, $errorControl,$textarea,$forlabel,$value,$nameid,$type,$required, $cssclass, $placeholder,$nameSimple, $label,$modal,$modal2,$readOnly;
    
    function getIcono() {
        return self::$icono;
    }

    function setIcono($icono) {
        self::$icono = $icono;
    }

    function getModal() {
        return self::$modal;
    }
    
    function getModal2() {
        return self::$modal2;
    }

    private function setInputImage($inputImage){
        self::$inputImage = $inputImage;
    }
    
    private function setInputImage2($inputImage){
        self::$inputImage2 = $inputImage;
    }
    
    function getInputImage(){
        return self::$inputImage;
    }
    
    function setBtnInput($btnInput){
        if (is_array($btnInput)) {
            self::$btnInput = "<span class='btn btn-".$btnInput['btncolor']."' id='btn".$btnInput['id']."' data-toggle='modal' data-target='#model".$btnInput['id']."' type='button'><span class='fa ".$btnInput['icono']."'></span> ".$btnInput['titulo']."</span>";
        }else{
            self::$btnInput = "";
        }
    }
    
    function getBtnInput(){
        return self::$btnInput;
    }
    
    function getInputImage2(){
        return self::$inputImage2;
    }
    
    function setModal($modal="") {
        if (is_array($modal)) {
            if (isset($modal['titulo'])) {
                $titulo = $modal['titulo'];
            }else{
                $titulo = "Buscar ".$this->getLabel();
            }
            if (isset($modal['btntitulo'])) {
                $btntitulo = $modal['btntitulo'];
            }else{
                $btntitulo = "Buscar";
            }
            if (isset($modal['icono'])) {
                $icono = $modal['icono'];
            }else{
                $icono = "fa fa-search";
            }
            if (isset($modal['btncolor'])) {
                $btncolor = $modal['btncolor'];
            }else{
                $btncolor = "default";
            }
            self::$modal = "<!-- Modal -->
                                <div class='modal fade bs-example-modal-lg' id='model".$modal['id']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                                  <div class='modal-dialog modal-lg' role='document'>
                                        <div class='modal-content'>
                                          <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                                <h4 class='modal-title' id='myModalLabel'> ".$titulo."</h4>
                                          </div>
                                          <div class='modal-body'>
                                                <form class='form-horizontal'>
                                                    <div class='form-group'>
                                                        <div class='col-sm-6'>
                                                          <input type='text' class='form-control' id='q".$modal['id']."' placeholder='Buscar ".$this->getLabel()."' onkeyup='".$modal['onclic']."(1)'>
                                                        </div>
                                                        <button type='button' class='btn btn-default' onclick='".$modal['onclic']."(1)'><span class='glyphicon glyphicon-search'></span> Buscar</button>
                                                    </div>
                                                </form>
                                                <div id='loader".$modal['id']."' style='position: absolute;	text-align: center; top: 55px; width: 100%;display:none;'></div><!-- Carga gif animado -->
                                                <div class='outer_div".$modal['id']."' ></div><!-- Datos ajax Final -->
                                          </div>
                                          <div class='modal-footer'>
                                                <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                                          </div>
                                        </div>
                                  </div>
                                </div>";
            if (isset($modal['inputModal'])) {
                if ($modal['inputModal']) {
                    $this->setInputImage("<span class='btn btn-".$btncolor."' id='btn".$modal['id']."' data-toggle='modal' data-target='#model".$modal['id']."' type='button'><span class='fa ".$icono."'></span> ".$btntitulo."</span>");
                }
            }
        }
        else{
            self::$modal = "";
            $this->setInputImage("");
        }
    }
    
    function setModal2($modal="") {
        if (is_array($modal)) {
            if (isset($modal['titulo'])) {
                $titulo = $modal['titulo'];
            }else{
                $titulo = "Buscar ".$this->getLabel();
            }
            if (isset($modal['btntitulo'])) {
                $btntitulo = $modal['btntitulo'];
            }else{
                $btntitulo = "Buscar";
            }
            if (isset($modal['icono'])) {
                $icono = $modal['icono'];
            }else{
                $icono = "fa fa-search";
            }
            if (isset($modal['btncolor'])) {
                $btncolor = $modal['btncolor'];
            }else{
                $btncolor = "default";
            }
            self::$modal2 = "<!-- Modal -->
                                <div class='modal fade bs-example-modal-lg' id='model".$modal['id']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                                  <div class='modal-dialog modal-lg' role='document'>
                                        <div class='modal-content'>
                                          <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                                <h4 class='modal-title' id='myModalLabel'>".$titulo."</h4>
                                          </div>
                                          <div class='modal-body'>
                                                <form class='form-horizontal'>
                                                    <div class='form-group'>
                                                        <div class='col-sm-6'>
                                                          <input type='text' class='form-control' id='q".$modal['id']."' placeholder='Buscar ".$this->getLabel()."' onkeyup='".$modal['onclic']."(1)'>
                                                        </div>
                                                        <button type='button' class='btn btn-default' onclick='".$modal['onclic']."(1)'><span class='glyphicon glyphicon-search'></span> Buscar</button>
                                                    </div>
                                                </form>
                                                <div id='loader".$modal['id']."' style='position: absolute;	text-align: center; top: 55px; width: 100%;display:none;'></div><!-- Carga gif animado -->
                                                <div class='outer_div".$modal['id']."' ></div><!-- Datos ajax Final -->
                                          </div>
                                          <div class='modal-footer'>
                                                <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                                          </div>
                                        </div>
                                  </div>
                                </div>";
            if (isset($modal['inputModal'])) {
                if ($modal['inputModal']) {
                    $this->setInputImage2("<span class='btn btn-".$btncolor."' id='btn".$modal['id']."' data-toggle='modal' data-target='#model".$modal['id']."' type='button'><span class='fa ".$icono."'></span> ".$btntitulo."</span>");
                }
            }
        }
        else{
            self::$modal2 = "";
            $this->setInputImage2("");
        }
    }
    
    function getOnClick() {
        return self::$onClick;
    }

    function setOnClick($onClick,$controller="",$accion="") {
        if (strlen($controller)>0 && strlen($accion)>0) {
            self::$onClick = "onclick='".$onClick."(\"".$controller."\",\"".$accion."\")'";
        }else{
            self::$onClick = "onclick='".$onClick."()'";
        }
    }
    
    function getSelectid() {
        return self::$selectid;
    }
    
    private function setSelectid($selectid) {
        self::$selectid = $selectid;
    }
    
    function getSelect() {
        return self::$select;
    }

    function setSelect($select,$value,$text) {
        if ($select <> "" && $value <> "" && $text <> "") {
            self::$select = array(
                "select"=>$select,
                "value"=>$value,
                "text"=>$text,
            );
        }else{
            self::$select = array(
                "select"=>"",
                "value"=>"",
                "text"=>"",
            );
        }
    }
    
    function getErrorText() {
        return self::$errorText;
    }

    function getErrorControl() {
        return self::$errorControl;
    }

    function setErrorText($errorText) {
        if ($errorText == '') {
            self::$errorText = "";
        }else{
            self::$errorText = "title='".$errorText."'";
        }
    }

    function setErrorControl($errorControl) {
        if ($errorControl == '') {
            self::$errorControl = "";
        }else{
            self::$errorControl = "pattern='".$errorControl."'";
        }
    }
    
    function getTextarea() {
        return self::$textarea;
    }

    function setTextarea($textarea) {
        self::$textarea = $textarea;
    }

    function getForlabel() {
        return self::$forlabel;
    }

    private function setForlabel($forlabel) {
        self::$forlabel = "for = '".$forlabel."'";
    }

    function getValue() {
        return self::$value;
    }

    function setValue($value) {
        if ($value == "") {
            self::$value = "";
        }else if ($this->getType() == "type = 'select'") {
            self::$value = $value;
        }else if ($this->getType() == "type = 'file'") {
                self::$value = $value;
            }else if ($this->getType() == "type = 'modal'") {
                    self::$value = $value;
                }else if ($this->getType() == "type = 'date'") {
                        self::$value = $value;
                    }else if ($this->getType() == "type = 'date2'") {
                        self::$value = $value;
                        }else{
                            self::$value = "value = '".$value."'";
                        }
    }

    function getNameid() {
        return self::$nameid;
    }

    function setNameid($nameid) {
        switch (self::$type) {
            case "type = 'select'":
                self::$nameid = "name = '".$nameid."' id = '". $nameid."'";
                $this->setSelectid($nameid);
                break;
            case "type = 'submit'":
                self::$nameid = "name = '".$nameid."'";
                break;
            case "type = 'button'":
                self::$nameid = "name = '".$nameid."'";
                break;
            case "type = 'modal'":
                self::$nameid = $nameid;
                break;
            default:
                self::setForlabel($nameid);
                self::setNameSimple($nameid);
                self::$nameid = "name = '".$nameid."' id = '". $nameid."'";
                break;
        }
    }

    function getReadOnly() {
        return self::$readOnly;
    }

    function setReadOnly($readOnly) {
        if ($readOnly) {
            self::$readOnly = "readonly";
        }else{
            self::$readOnly = "";
        }
    }
    
    function getNameSimple() {
        return self::$nameSimple;
    }

    function setNameSimple($nameSimple) {
        self::$nameSimple = $nameSimple;
    }
    
    function getType() {
        return self::$type;
    }

    function setType($type) {
        self::$type = "type = '" . $type."'";
    }

    function getRequired() {
        return self::$required;
    }

    function getCssclass() {
        return self::$cssclass;
    }

    function getPlaceholder() {
        return self::$placeholder;
    }

    function getLabel() {
        return self::$label;
    }

    function setRequired($required) {
        self::$required = "required = '".$required."'";
    }

    function setCssclass($cssclass) {
        self::$cssclass = "class = '" .$cssclass."'";
    }

    function setPlaceholder($placeholder) {
        self::$placeholder = " placeholder = '" .$placeholder."'";
    }

    function setLabel($label) {
        self::$label = $label;
    }
}