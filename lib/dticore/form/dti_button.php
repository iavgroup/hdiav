<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *
 */
class dti_button {

    private static $ctlVariables,$btn;

    public function __construct() {
        self::$btn = '';
        if (!isset(self::$ctlVariables)) {
            //$variables = new \dti_core("formvalidate");
            self::$ctlVariables = 0;
        }
    }
    
    /**
     *
     * @param array $dt titulo,color(verde,azul,rojo,celeste,plomo,amarrillo),clic
     */
    public function setButton($dt){
        $color = 'default';
        switch ($dt['color']) {
            case 'verde':$color = 'success'; break;
            case 'azul':$color = 'primary'; break;
            case 'rojo':$color = 'danger'; break;
            case 'celeste':$color = 'info'; break;
            case 'plomo':$color = 'default'; break;
            case 'amarillo':$color = 'warning'; break;
        }
        if (isset($dt['clic'])) {
            self::$btn .= '     <button type="button" onclick="'.$dt['clic'].'" class="btn btn-'.$color.'">'.$dt['titulo'].'</button>';
        }else{
            self::$btn .= '     <button type="button" class="btn btn-'.$color.'">'.$dt['titulo'].'</button>';
        }
    }
    
    public function getButton(){
        return self::$btn;
    }
}