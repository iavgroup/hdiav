<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_form extends dti_form_element {
    
    private static $elementos,$form,$group,$recorrer,$ctlVariables,$conForm;

    public function __construct($accion,$metodo,$encrypt,$group,$conForm) {
        //Verificar si se va a trabajar con formulario
        self::$conForm = $conForm;
        //Limpiar Variables
        self::$elementos = null;
        $this->setLabel("");
        if (self::$conForm) {
            self::$form = "<form action='".$accion."' class='form-horizontal ws-validate' method='".$metodo."' enctype='".$encrypt."'>";
            self::$group = $group;
            self::$recorrer = 0;
            self::dti_group("INI");
            //Cargas Css/Js/Script Obligatorios
            if (!isset(self::$ctlVariables)) {
                $variables = new \dti_core("formvalidate");
                self::$ctlVariables = 0;
            }
        }else{
            //Cargas Css/Js/Script Obligatorios
            if (!isset(self::$ctlVariables)) {
                $variables = new \dti_core("formvalidate");
                self::$ctlVariables = 0;
            }
        }
    }

    public function addelement($elemento,$version=2){
        //Elementos que no van a sumar
        if ($elemento->getType() <> "type = 'hidden'") {
            self::$recorrer ++;
        }
        //Realizar acciones de Acuerdo a lo que necesitamos crear
        switch ($elemento->getType()) {
            case "type = 'hidden'":
                self::dti_hidden($elemento);
                break;
            case "type = 'text'":
                self::dti_input($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'textarea'":
                self::dti_textarea($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'email'":
                self::dti_input($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'number'":
                self::dti_input($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'password'":
                self::dti_input($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'date'":
                self::dti_date($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'date2'":
                self::dti_date2($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'file'":
                self::dti_file($elemento,$version);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'multifile'":
                self::dti_multifile($elemento,$version);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'checkbox'":
                self::dti_checkbox($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'select'":
                self::dti_select($elemento);
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                    self::$recorrer = 0;
                    self::dti_group("INI");
                }
                break;
            case "type = 'modal'":
                self::dti_modal($elemento);
                if (self::$conForm) {
                    if (self::$recorrer == self::$group) {
                        self::dti_group("FIN");
                        self::$recorrer = 0;
                        self::dti_group("INI");
                    }
                }
                break;
            case "type = 'submit'":
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                }
                self::dti_button($elemento);
                break;
            case "type = 'button'":
                if (self::$recorrer == self::$group) {
                    self::dti_group("FIN");
                }
                self::dti_button($elemento);
                break;
        }
        //Limpiar Variables Criticas
        $this->setErrorControl("");
        $this->setErrorText("");
        $this->setReadOnly(false);
        $this->setValue("");
    }
    
    private function dti_group($dato){
        if ($dato == "INI") {
            self::$elementos .= "<div class='form-group row'>";
        }else{
            self::$elementos .= "</div>";
        }
    }
    
    private function dti_hidden($datos){
        $componente = "<input ".$datos->getType()." ".$datos->getCssClass()." ".$datos->getNameid()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getValue()." ".$datos->getErrorText()."  ".$datos->getErrorControl()."  />";
        self::$elementos .= $componente;
    }
    
    private function dti_input($datos){
        if ($datos->getLabel() == "") {
            if (strlen($this->getErrorControl())>12 && strlen($this->getErrorText())>9) {
                $componente = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                    <input ".$datos->getType()." ".$datos->getCssClass()." ".$datos->getNameid()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getValue()." ".$datos->getErrorText()."  ".$datos->getErrorControl()." ".$datos->getReadOnly()."  />";
                if (strlen($this->getInputImage())>2) {
                    $componente .= $this->getInputImage();
                }
                if (strlen($this->getInputImage2())>2) {
                    $componente .= $this->getInputImage2();
                }
                if (strlen($this->getBtnInput())>2) {
                    $componente .= $this->getBtnInput();
                }
                $componente .= "</div>";
            }else{
                $componente = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                <input ".$datos->getType()." ".$datos->getCssClass()." ".$datos->getNameid()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getValue()." ".$datos->getReadOnly()." />";
                if (strlen($this->getInputImage())>2) {
                    $componente .= $this->getInputImage();
                }
                if (strlen($this->getInputImage2())>2) {
                    $componente .= $this->getInputImage2();
                }
                if (strlen($this->getBtnInput())>2) {
                    $componente .= $this->getBtnInput();
                }
                $componente .= "</div>";
            }
        }
        else{
            if (strlen($this->getErrorControl())>12 && strlen($this->getErrorText())>9) {
                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <input ".$datos->getType()." ".$datos->getCssClass()." ".$datos->getNameid()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getValue()." ".$datos->getErrorText()."  ".$datos->getErrorControl()." ".$datos->getReadOnly()." />";
                            if (strlen($this->getInputImage())>0) {
                                $componente .= $this->getInputImage();
                            }
                            if (strlen($this->getInputImage2())>0) {
                                $componente .= $this->getInputImage2();
                            }
                            if (strlen($this->getBtnInput())>2) {
                                $componente .= $this->getBtnInput();
                            }
                            $componente .= "</div>";
            }else{
                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <input ".$datos->getType()." ".$datos->getCssClass()." ".$datos->getNameid()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getValue()." ".$datos->getReadOnly()." />";
                            if (strlen($this->getInputImage())>0) {
                                $componente .= $this->getInputImage();
                            }
                            if (strlen($this->getInputImage2())>0) {
                                $componente .= $this->getInputImage2();
                            }
                            if (strlen($this->getBtnInput())>2) {
                                $componente .= $this->getBtnInput();
                            }
                            $componente .= "</div>";
            }
        }
        //Verificar si configuro un Modal
        if (strlen($this->getModal())>0) {
            $componente .= $this->getModal();
        }
        if (strlen($this->getModal2())>0) {
            $componente .= $this->getModal2();
        }
        self::$elementos .= $componente;
        $this->setModal(null);
        $this->setModal2(null);
        $this->setBtnInput("");
        $this->setErrorControl("");
        $this->setErrorText("");
    }

    private function dti_date($datos){

        $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
            <div class='input-group date form_date col-lg-4 col-md-4 col-sm-4 col-xs-12' data-date='' data-date-format='yyyy/mm/dd' data-link-field='".$this->getNameSimple()."' data-link-format='yyyy-mm-dd'>
                <input ".$datos->getCssClass()." size='12' type='text' value='".$datos->getValue()."' readonly>
                <span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>
                <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
            </div>
            <input type='hidden' id='".$this->getNameSimple()."' value='' /><br/>";
                    
        //<input ".$datos->getType()." ".$datos->getCssClass()." ".$datos->getNameid()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getValue()." ".$datos->getErrorText()."  ".$datos->getErrorControl()."  />
        
        self::$elementos .= $componente;
    }
    
    private function dti_date2($datos){

        $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
            <div class='input-group date form_date col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                <input type='text' id='".$this->getNameSimple()."' value='".$datos->getValue()."' readonly ".$datos->getCssClass().">            
                <span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>
                <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
            </div>";
            
        self::$elementos .= $componente;
    }
    
    private function dti_checkbox($datos){
        if ($datos->getValue() != null) {
            if ($datos->getValue() == "value = '1'") {
                $check = "checked";
            }else{
                $check = "";
            }
            }else {$check = "";}
        $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                <input ".$datos->getType()." ".$datos->getNameid()." ".$check." data-toggle='toggle' data-on='SI' data-off='NO' data-onstyle='success' data-offstyle='danger'  />
            </div>";
        self::$elementos .= $componente;
    }
    
    private function dti_select($datos){
        $option = "";
        if (\config\globalFunctions::es_bidimensional($datos->getSelect()["select"])) {
            if ($datos->getValue() != null) {
                dti_core::set("script", "<script> $('#".$datos->getSelectid()."').val('".$datos->getValue()."'); </script>");
            }
            foreach ($datos->getSelect()["select"] as $key => $value) {
                $option .= "<option data-tokens='".$value[$datos->getSelect()["text"]]."' value='".$value[$datos->getSelect()["value"]]."' >".$value[$datos->getSelect()["text"]]."</option>";
            }
            
            $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                    <select class='selectpicker' data-live-search='true' ".$datos->getNameid()." >
                        ".$option."
                    </select>
                </div>";
            self::$elementos .= $componente;
        }else{
            if (isset($datos->getSelect()["select"][$datos->getSelect()["value"]])) {
                if ($datos->getValue() != null) {
                    dti_core::set("script", "<script> $('#".$datos->getSelectid()."').val('".$datos->getValue()."'); </script>");
                }

                $option .= "<option data-tokens='".$datos->getSelect()["select"][$datos->getSelect()["text"]]."' value='".$datos->getSelect()["select"][$datos->getSelect()["value"]]."' >".$datos->getSelect()["select"][$datos->getSelect()["text"]]."</option>";

                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                    <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                        <select class='selectpicker' data-live-search='true' ".$datos->getNameid()." >
                            ".$option."
                        </select>
                    </div>";
                self::$elementos .= $componente;
            }
        }
    }
    
    private function dti_file($datos,$version='1'){
        switch ($version) {
            case '1':
                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <input ".$datos->getType()." ".$datos->getNameid()." class='filestyle' data-placeholder='".$datos->getValue()."' data-buttonText='Seleccionar ".$datos->getLabel()."' data-buttonName='btn-primary' ".$datos->getRequired()." >
                            </div>";
                break;
            case '2':
                //Validar si el valor es > 0
                if (strlen($datos->getValue())>0) {
                    $ruta = "src='".$datos->getValue()."'";
                }else{
                    $ruta = "";
                }
                
                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <div class='input-group'>
                                    <span class='input-group-btn'>
                                        <span class='btn btn-default btn-file'>
                                            Seleccionar ".$datos->getLabel()." <input ".$datos->getType()." ".$datos->getNameid()." value='".$datos->getValue()."' >
                                        </span>
                                    </span>
                                    <input type='text' class='form-control' value='".$datos->getValue()."' readonly>
                                </div>
                                <img id='img".$this->getNameSimple()."' width='100px' height='100px' ".$ruta."/>
                            </div>";

                $componente .= "<script>
                                    $(document).ready( function() {

                                        function readURL(input,img) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    $('#'+img).attr('src', e.target.result);
                                                }

                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }

                                        $('#". $this->getNameSimple()."').change(function(){
                                            readURL(this,'img".$this->getNameSimple()."');
                                        });
                                    });
                                </script>";
                break;
        }
        self::$elementos .= $componente;
    }
    
    private function dti_multifile($datos,$version='1'){
        switch ($version) {
            case '1':
                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <input ".$datos->getType()." ".$datos->getNameid()." class='filestyle' data-placeholder='".$datos->getValue()."' data-buttonText='Seleccionar ".$datos->getLabel()."' data-buttonName='btn-primary' ".$datos->getRequired()." >
                            </div>";
                break;
            case '2':
                //Validar si el valor es > 0
                if (strlen($datos->getValue())>0) {
                    $ruta = "src='".$datos->getValue()."'";
                }else{
                    $ruta = "";
                }
                
                $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <div class='input-group'>
                                    <span class='input-group-btn'>
                                        <span class='btn btn-default btn-file'>
                                            Seleccionar ".$datos->getLabel()." <input ".$datos->getType()." ".$datos->getNameid()." value='".$datos->getValue()."' >
                                        </span>
                                    </span>
                                    <input type='text' class='form-control' value='".$datos->getValue()."' readonly>
                                </div>
                                <img id='img".$this->getNameSimple()."' width='100px' height='100px' ".$ruta."/>
                            </div>";

                $componente .= "<script>
                                    $(document).ready( function() {

                                        function readURL(input,img) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    $('#'+img).attr('src', e.target.result);
                                                }

                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }

                                        $('#". $this->getNameSimple()."').change(function(){
                                            readURL(this,'img".$this->getNameSimple()."');
                                        });
                                    });
                                </script>";
                break;
        }
        self::$elementos .= $componente;
    }
    
    private function dti_textarea($datos){
        $componente = "<label ".$datos->getForlabel()." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                <textarea rows='4' cols='50' ".$datos->getNameid()." ".$datos->getCssClass()." ".$datos->getPlaceholder()." ".$datos->getRequired()." ".$datos->getErrorText()."  ".$datos->getErrorControl()." >".$datos->getTextarea()."</textarea>
            </div>";
        self::$elementos .= $componente;
        //Limpiar los datos
        $this->setErrorControl("");
        $this->setErrorText("");
    }
    
    private function dti_modal($datos){
        $componente = "<label for='".$datos->getNameid()."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos->getLabel()."</label>
            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                <button type='button' class='btn btn-warning col-md-12 col-sm-12' id=".$datos->getNameid()." name=".$datos->getNameid()." data-toggle='modal' data-target='#model".$datos->getNameid()."'><span class='fa ".$datos->getIcono()."'></span> ".$datos->getValue()."</button>
            </div>";
        if ($datos->getModal() != null) {
            $componente .= $datos->getModal();
        }
        if ($datos->getModal2() != null) {
            $componente .= $datos->getModal2();
        }
        self::$elementos .= $componente;
        //Limpiar los datos
        $this->setModal(null);
        $this->setModal2(null);
    }

    private function dti_button($datos){
        $componente = "<div class='form-group row'><div class='col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12'>
                            <input ".$datos->getType()." ".$datos->getNameid()." ".$datos->getValue()." ".$datos->getOnClick()." ".$datos->getCssClass()." />
                        </div></div>";
        self::$elementos .= $componente;
    }
    
    public function getForm(){
        //Crea Botones Independientes Modal
        if (self::$conForm) {
            if (self::$recorrer <> self::$group) {
                self::dti_group("FIN");
            }

            $formulario = "<div id='_AJAX_ERROR_'></div>";
            $formulario .= self::$form;
            $formulario .= self::$elementos;
            $formulario .= "</form>";

            return $formulario;
        }else{
            $formulario = self::$elementos;
            return $formulario;
        }
    }
}