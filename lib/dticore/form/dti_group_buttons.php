<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_group_buttons {

    private static $groupbtn,$ctlVariables,$detalle,$confirmacion,$script;

    /**
     * Función: Crea grupo de botones
     * 
     * @param array $dt Ej: 'clic'=>'saveCliente',
                            'icono'=>'fa fa-usd',
                            'titulo'=>'Guardar',
                            'btntitulo'=>'Confirmar',
                            'btnmensaje'=>'Desea Guardar el Cliente?',
                            'btn'=>array(['titulo'=>'SI','comandos'=>'setJsonCliente();'],
                                        ['titulo'=>'NO','comandos'=>'']),
     */
    function setGroupButtons($dt){
        if (isset($dt['enlace'])) {
            self::$detalle .= '<li><a onclick="location.href=\''.$dt['clic'].'\'" role="tab" data-toggle="tab"><i class="'.$dt['icono'].'"></i>'.$dt['titulo'].'</a></li>';
        }else{
            self::$detalle .= '<li><a onclick="'.$dt['clic'].'()" role="tab" data-toggle="tab"><i class="'.$dt['icono'].'"></i>'.$dt['titulo'].'</a></li>';
            self::$confirmacion = config\globalFunctions::getMsgConfirmJS(array(
                'funcion'=>$dt['clic'],
                'titulo'=>$dt['btntitulo'],
                'mensaje'=>$dt['btnmensaje'],
                'btn'=>$dt['btn'],
            ));
            self::$script .= self::$confirmacion;
        }
    }

    public function __construct() {
        //Limpiamos las variables para volver a llamar
        self::$detalle = '';
        self::$script = '';
        self::$groupbtn = '';
        self::$confirmacion = '';
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            //$variables = new \dti_core("panel");
            self::$ctlVariables = 0;
        }
    }

    public function getGroupButtons(){
        self::$groupbtn .= '<div class="dti_section"><ul class="nav nav-pills nav-pills-warning" role="tablist">';
        self::$groupbtn .= self::$detalle;
        self::$groupbtn .= '</ul></div>';
        
        return self::$groupbtn.self::$script;
    }
}
