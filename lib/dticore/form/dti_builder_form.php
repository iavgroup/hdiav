<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *
 */
class dti_builder_form extends \core\EntidadBase {

    private static $ctlVariables,$form,$elementos,$recorrer,$modal,$script;
    public $adapter;

    public function __construct($adapter) {
        $this->adapter = $adapter;
        $table='dti_form_detalle';
	parent::__construct($table,$adapter);
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("formvalidate");
            self::$ctlVariables = 0;
        }
    }

    public function setForm($maestro,$ordenarX,$get='',$crud=''){
        self::$recorrer = 0;
        self::$elementos = "";
        self::$modal = "";
        self::$script = "";
        $cssValidacion = '';
        $cssForm = '';
        //Validar si tiene validaciones
        if ($maestro['validacion'] == 1) {
            $cssValidacion = 'ws-validate';
        }
        //Validar si tiene validaciones
        if (strlen($maestro['css']) > 1) {
            $cssForm = $maestro['css'];
        }
        //Validar si es con formulario
        if ($maestro['conform'] == 1) {
            //Si es con formulario
            self::$form = "<form action='".$maestro['accion']."' class='".$cssForm.$cssValidacion." ws-validate' method='".$maestro['metodo']."' enctype='".$maestro['encrypt']."'>";
        }else{
            //No es con formulario
            self::$form = "";
        }
        //Armar los elementos
        $detalle = $this->getByOrderBy('idform', $maestro['id'],$ordenarX);
        foreach ($detalle as $key => $componente) {
            $this->addElement($maestro['columnas'],$maestro['version'],$componente,$get,$crud,$maestro['entidad'],$maestro['columnid'],$maestro['columnid2']);
        }
    }

    public function getForm(){
        if (self::$recorrer <> 0) $this->agrupacion("FIN",'1');
        $formulario = self::$form.self::$elementos."</form>".self::$modal.self::$script;
        return $formulario;
    }

    public function addElement($columnas,$version,$detalle,$get='',$crud='',$entidad='',$columnid='',$columnid2=''){
        if (self::$recorrer == $columnas) $this->agrupacion("FIN",$version);
        if (self::$recorrer == 0) $this->agrupacion("INI",$version);
        switch ($detalle['tipo']) {
            case 'text':
                self::$elementos .= $this->newText($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'textarea':
                self::$elementos .= $this->newTextarea($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'email':
                self::$elementos .= $this->newText($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'number':
                self::$elementos .= $this->newText($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'password':
                self::$elementos .= $this->newText($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'checkbox':
                self::$elementos .= $this->newCheckbox($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'date':
                self::$elementos .= $this->newDate($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'file':
                self::$elementos .= $this->newFile($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'select':
                self::$elementos .= $this->newSelect($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                self::$recorrer++;
                break;
            case 'hidden':
                self::$elementos .= $this->newHidden($detalle,$version,$get,$crud,$entidad,$columnid,$columnid2);
                break;
        }
    }

    private function agrupacion($dato,$version){
        if ($dato == "INI") {
            switch ($version) {
                case '1':
                    self::$elementos .= "<div class='form-group row'>";
                    break;
                case '2':
                    self::$elementos .= "<div class='form-group row mat-div'>";
                    break;
            }
        }else{
            self::$elementos .= "</div>";
            self::$recorrer = 0;
        }
    }

    private function newText($datos,$version,$get='',$crud='',$endidad='',$columnid='',$columnid2=''){
        //Declaracion de Variables
        $componente = "";
        $readonly = "";
        $requerido = "";
        $errorText = "";
        $errorControl = "";
        $valor = "";
        $linklabel = "";
        //POner Valores en caso de que tenga id o crud
        if (strlen($get)>1 && strlen($endidad) > 1) {
            $model = '\\Entidades\\'.$endidad;
            $mget = new $model($this->adapter);
            if (strlen($crud)>1) $dtget = $mget->getBy2($columnid,$get,$columnid2,$crud);
            else $dtget = $mget->getByTop1($columnid,$get);
        }
        //Solo lectura en caso de que tenga activado
        if (($datos['readonlyid'] == 1 && strlen($get)>1) || ($datos['readonlycrud'] == 1 && strlen($crud)>1)) {
            $readonly = "readonly";
        }
        if ($datos['readonly'] == 1) $readonly = "readonly";
        if ($datos['requerido'] == 1) $requerido = "required = 'true'";
        if (strlen($datos['errortext']) > 1) $errorText = "title='".$datos['errortext']."'";
        if (strlen($datos['errorcontrol']) > 1) $errorControl = "pattern='".$datos['errorcontrol']."'";
        if (strlen($datos['valor']) > 1) $valor = "value = '".$datos['valor']."'";
        if (strlen($get)>1) $valor = "value = '".$dtget[$datos['bdd']]."'";
        if (strlen($datos['linkbutton']) > 1) {
            if ($datos['linktipo'] == 'FORM') {
                $linklabel = "<a class='linklabel' href='template/".$datos['linkparametro']."/$get'><label class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label></a>";
            }else{
                $linklabel = "<a class='linklabel' data-toggle='modal' data-target='#model".$datos['nameid']."'><label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label></a>";
                self::$modal .= $this->newModal($datos,$get);
            }
        }else{
            $linklabel = "<label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>";
        }
        switch ($version) {
            case '1':
                if (strlen($datos['icono']) > 1) {
                    $componente .= $linklabel . //"<label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                                "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                    <div class='input-group'>
                                        <input type='".$datos['tipo']."' class='".$datos['css']."' id='".$datos['nameid']."' name='".$datos['nameid']."' placeholder='".$datos['placeholder']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl." ".$valor." />";
                       if (strlen($datos['modal']) > 1) {
                            self::$modal .= $this->newModal($datos,$get);
                            $componente .= "<span class='input-group-addon mat-img' id='btn".$datos['nameid']."' data-toggle='modal' data-target='#model".$datos['nameid']."' type='button'><span class='".$datos['icono']."'></span></span>";
                        }else{
                            $componente .= "<a class='input-group-addon mat-img' id='basic-".$datos['nameid']."'><i class='".$datos['icono']."'></i></a>";
                        }
                    $componente .= "</div>
                                </div>";
                }else{
                    $componente .= $linklabel . //"<label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                                "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                    <input type='".$datos['tipo']."' class='".$datos['css']."' id='".$datos['nameid']."' name='".$datos['nameid']."' placeholder='".$datos['placeholder']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl." ".$valor." />
                                </div>";
                }
                break;
            case '2':
                if (strlen($datos['icono']) > 1) {
                    $componente .= "<label for='".$datos['nameid']."' class='mat-label'>".$datos['titulo']."</label>
                                <div class='input-group'>
                                    <input type='".$datos['tipo']."' id='".$datos['nameid']."' name='".$datos['nameid']."' class='mat-input' aria-describedby='basic-".$datos['nameid']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl." ".$valor." >";
                        if (strlen($datos['modal']) > 1) {
                            self::$modal .= $this->newModal($datos,$get);
                            $componente .= "<span class='input-group-addon mat-img' id='btn".$datos['nameid']."' data-toggle='modal' data-target='#model".$datos['nameid']."' type='button'><span class='".$datos['icono']."'></span></span>";
                        }else{
                            $componente .= "<a class='input-group-addon mat-img' id='basic-".$datos['nameid']."'><i class='".$datos['icono']."'></i></a>";
                        }
                    $componente .= "</div>";
                }else{
                    $componente .= "<label for='".$datos['nameid']."' class='mat-label'>".$datos['titulo']."</label>
                                <div class='input-group'>
                                    <input type='".$datos['tipo']."' id='".$datos['nameid']."' name='".$datos['nameid']."' class='mat-input' aria-describedby='basic-".$datos['nameid']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl." ".$valor." >
                                </div>";
                }
                break;
        }
        return $componente;
    }

    private function newTextarea($datos,$version,$get='',$crud='',$endidad='',$columnid='',$columnid2=''){
        $componente = "";
        $readonly = "";
        $requerido = "";
        $errorText = "";
        $errorControl = "";
        $valor = "";
        if ($datos['readonly'] == 1) $readonly = "readonly";
        if ($datos['requerido'] == 1) $requerido = "required = 'true'";
        if (strlen($datos['errortext']) > 1) $errorText = "title='".$datos['errortext']."'";
        if (strlen($datos['errorcontrol']) > 1) $errorControl = "pattern='".$datos['errorcontrol']."'";
        if (strlen($datos['valor']) > 1) $valor = "value = '".$datos['valor']."'";
        switch ($version) {
            case '1':
                if (strlen($datos['icono']) > 1) {
                    $componente .= "<label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                    <div class='input-group'>
                                        <textarea rows='4' cols='50' class='".$datos['css']."' id='".$datos['nameid']."' name='".$datos['nameid']."' placeholder='".$datos['placeholder']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl." > ".$valor." </textarea>";
                       if (strlen($datos['modal']) > 1) {
                            self::$modal .= $this->newModal($datos);
                            $componente .= "<span class='input-group-addon mat-img' id='btn".$datos['nameid']."' data-toggle='modal' data-target='#model".$datos['nameid']."' type='button'><span class='".$datos['icono']."'></span></span>";
                        }else{
                            $componente .= "<a class='input-group-addon mat-img' id='basic-".$datos['nameid']."'><i class='".$datos['icono']."'></i></a>";
                        }
                    $componente .= "</div>
                                </div>";
                }else{
                    $componente .= "<label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                    <textarea rows='4' cols='50' class='".$datos['css']."' id='".$datos['nameid']."' name='".$datos['nameid']."' placeholder='".$datos['placeholder']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl."> ".$valor." </textarea>
                                </div>";
                }
                break;
            case '2':
                if (strlen($datos['icono']) > 1) {
                    $componente .= "<label for='".$datos['nameid']."' class='mat-label'>".$datos['titulo']."</label>
                                <div class='input-group'>
                                    <textarea rows='4' cols='50' id='".$datos['nameid']."' name='".$datos['nameid']."' class='mat-input' aria-describedby='basic-".$datos['nameid']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl." > ".$valor." </textarea>";
                        if (strlen($datos['modal']) > 1) {
                            self::$modal .= $this->newModal($datos);
                            $componente .= "<span class='input-group-addon mat-img' id='btn".$datos['nameid']."' data-toggle='modal' data-target='#model".$datos['nameid']."' type='button'><span class='".$datos['icono']."'></span></span>";
                        }else{
                            $componente .= "<a class='input-group-addon mat-img' id='basic-".$datos['nameid']."'><i class='".$datos['icono']."'></i></a>";
                        }
                    $componente .= "</div>";
                }else{
                    $componente .= "<label for='".$datos['nameid']."' class='mat-label'>".$datos['titulo']."</label>
                                <div class='input-group'>
                                    <textarea rows='4' cols='50' id='".$datos['nameid']."' name='".$datos['nameid']."' class='mat-input' aria-describedby='basic-".$datos['nameid']."' ".$readonly." ".$requerido." ".$errorText." ".$errorControl."> ".$valor." </textarea>
                                </div>";
                }
                break;
        }
        return $componente;
    }

    private function newModal($datos,$get=''){
        $post = '';
        $accion = '';
        switch ($datos['linktipo']) {
            case 'MODAL':
                $modal = "<!-- Modal FORM -->
                    <div class='modal fade bs-example-modal-lg' id='model".$datos['nameid']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                      <div class='modal-dialog modal-lg' role='document'>
                            <div class='modal-content'>
                              <div class='modal-header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                    <h4 class='modal-title' id='myModalLabel'>".$datos['titulo']."</h4>
                              </div>
                              <div class='modal-body'>
                                    <div id='loader".$datos['nameid']."' style='position: absolute;	text-align: center; top: 55px; width: 100%;display:none;'></div><!-- Carga gif animado -->
                                    <div class='outer_div".$datos['nameid']."' ></div><!-- Datos ajax Final -->
                              </div>
                              <div class='modal-footer'>
                                    <button type='button' class='btn btn-primary btn-small' data-dismiss='modal'>Cerrar</button>
                              </div>
                            </div>
                      </div>
                    </div>";
                if (strlen($get)>1) {
                    $post = ',id: '.$get.'';
                }
                if (strlen($datos['accion']) > 1) {
                    $accion = ',accion: '.$datos['accion'].'';
                }
                self::$script .= '<script type="text/javascript">$(document).ready(function(){ '.$datos['nameid'].'(); });
                                        function '.$datos['nameid'].'(){
                                        var q= $("#'.$datos['parametro2'].'").val();
                                        $("#loader'.$datos['nameid'].'").fadeIn("slow");
                                        $.ajax({
                                                //Escogemos la URL donde vamos a buscar.
                                                url:"'.CONTROLADOR_DEFECTO.'/'.$datos['linkparametro'].'/",
                                                //Escogemos el metodo de envio en esta caso POST.
                                                data:{val: q '.$post.' '.$accion.'},
                                                type: "post",
                                                //Mostramos una imagen y la palabra cargando mientras espera.
                                                beforeSend: function(){
                                                    $("#loader'.$datos['nameid'].'").html("<img src=\'public/images/ajax-loader.gif\'> Cargando...");
                                                },
                                                //Una vez que termino mostramos los datos y limpiamos el cargando.
                                                success:function(data){
                                                    $(".outer_div'.$datos['nameid'].'").html(data).fadeIn("slow");
                                                    $("#loader'.$datos['nameid'].'").html("");
                                                }
                                            });
                                    }</script>';
                break;
            default:
                $modal = "<!-- Modal -->
                <div class='modal fade bs-example-modal-lg' id='model".$datos['nameid']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
                  <div class='modal-dialog modal-lg' role='document'>
                        <div class='modal-content'>
                          <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <h4 class='modal-title' id='myModalLabel'>".$datos['titulo']."</h4>
                          </div>
                          <div class='modal-body'>
                                <div class='input-group row'>
                                    <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                                        <input type='text' class='form-control' id='q".$datos['nameid']."' placeholder='Buscar ".$datos['titulo']."' onkeyup='".$datos['modal']."(1)'>
                                    </div>
                                    <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                                        <button type='button' class='btn btn-info' onclick='".$datos['modal']."(1)'><span class='glyphicon glyphicon-search'></span> Buscar</button>
                                    </div>
                                </div>
                                <div id='loader".$datos['nameid']."' style='position: absolute;	text-align: center; top: 55px; width: 100%;display:none;'></div><!-- Carga gif animado -->
                                <div class='outer_div".$datos['nameid']."' ></div><!-- Datos ajax Final -->
                          </div>
                          <div class='modal-footer'>
                                <button type='button' class='btn btn-primary btn-small' data-dismiss='modal'>Cerrar</button>
                          </div>
                        </div>
                  </div>
                </div>";
                if (strlen($datos['accion']) > 1) {
                        $accion = ',accion: "'.$datos['accion'].'"';
                    }
                if (strlen($datos['parametro2']) > 1) {
                    self::$script .= '<script type="text/javascript">function '.$datos['modal'].'(page=1){
                                        //Cojemos la variable.
                                        var q= $("#q'.$datos['nameid'].'").val();
                                        var w= $("#'.$datos['parametro2'].'").val();
                                        $("#loader'.$datos['nameid'].'").fadeIn("slow");
                                        $.ajax({
                                                //Escogemos la URL donde vamos a buscar.
                                                url:"'.CONTROLADOR_DEFECTO.'/'.$datos['parametro'].'/",
                                                //Envialos los parametros.
                                                data: {page: page,q: q,w: w '.$accion.'},
                                                //Escogemos el metodo de envio en esta caso POST.
                                                type: "post",
                                                //Mostramos una imagen y la palabra cargando mientras espera.
                                                beforeSend: function(){
                                                    $("#loader'.$datos['nameid'].'").html("<img src=\'public/images/ajax-loader.gif\'> Cargando...");
                                                },
                                                //Una vez que termino mostramos los datos y limpiamos el cargando.
                                                success:function(data){
                                                    $(".outer_div'.$datos['nameid'].'").html(data).fadeIn("slow");
                                                    $("#loader'.$datos['nameid'].'").html("");
                                                }
                                            });
                                    }</script>';
                }else{
                    self::$script .= '<script>function '.$datos['modal'].'(page=1){
                                        //Cojemos la variable.
                                        var q= $("#q'.$datos['nameid'].'").val();
                                        $("#loader'.$datos['nameid'].'").fadeIn("slow");
                                        $.ajax({
                                                //Escogemos la URL donde vamos a buscar.
                                                url:"'.CONTROLADOR_DEFECTO.'/'.$datos['parametro'].'/",
                                                //Envialos los parametros.
                                                data: {page: page,q: q '.$accion.'},
                                                //Escogemos el metodo de envio en esta caso POST.
                                                type: "post",
                                                //Mostramos una imagen y la palabra cargando mientras espera.
                                                beforeSend: function(){
                                                    $("#loader'.$datos['nameid'].'").html("<img src=\'public/images/ajax-loader.gif\'> Cargando...");
                                                },
                                                //Una vez que termino mostramos los datos y limpiamos el cargando.
                                                success:function(data){
                                                    $(".outer_div'.$datos['nameid'].'").html(data).fadeIn("slow");
                                                    $("#loader'.$datos['nameid'].'").html("");
                                                }
                                            });
                                    }</script>';
                }
                break;
        }
        return $modal;
    }

    private function newHidden($datos,$version,$get='',$crud='',$endidad='',$columnid='',$columnid2=''){
        $componente = "";
        $valor = "";
        if (strlen($datos['valor']) > 1) $valor = "value = '".$datos['valor']."'";
        switch ($version) {
            case '1':
                $componente .= "<input type='".$datos['tipo']."' class='".$datos['css']."' id='".$datos['nameid']."' name='".$datos['nameid']."' placeholder='".$datos['placeholder']."' ".$valor." />";
                break;
            case '2':
                $componente .= "<input type='".$datos['tipo']."' id='".$datos['nameid']."' name='".$datos['nameid']."' class='mat-input' aria-describedby='basic-".$datos['nameid']."' ".$valor." >";
                break;
        }
        return $componente;
    }

    private function newCheckbox($datos,$version,$get='',$crud='',$endidad='',$columnid='',$columnid2=''){
        $componente = "";
        $valor = "";
        $check = "data-on='NO' data-off='SI'";
        if (strlen($datos['valor']) == '1') $check = "data-on='SI' data-off='NO'";
        switch ($version) {
            case '1':
                $componente = "<label ".$datos['nameid']." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                    <label class='dti_switch'>
                                    <input class='switch-input' type='".$datos['tipo']."' />
                                    <span class='switch-label' ".$check."></span> <span class='switch-handle'></span></label>
                                </div>";
                break;
            case '2':
                $componente = "<label ".$datos['nameid']." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                    <label class='dti_switch'>
                                    <input class='switch-input' type='".$datos['tipo']."' />
                                    <span class='switch-label' ".$check."></span> <span class='switch-handle'></span></label>
                                </div>";
                break;
        }
        return $componente;
    }

    private function newDate($datos,$version,$get='',$crud='',$endidad='',$columnid='',$columnid2=''){
        $componente = "<label ".$datos['nameid']." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
            <div class='input-group date form_date col-lg-4 col-md-4 col-sm-4 col-xs-12' data-date='' data-date-format='yyyy/mm/dd' data-link-field='".$datos['nameid']."' data-link-format='yyyy-mm-dd'>
                <input class='".$datos['css']."' size='12' type='text' value='".$datos['valor']."' readonly>
                <span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>
                <span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>
            </div>
            <input type='hidden' id='".$datos['nameid']."' value='' /><br/>";
                    
        return $componente;
    }

    private function newFile($datos,$version,$get='',$crud='',$endidad='',$columnid='',$columnid2=''){
        switch ($version) {
            case '1':
                $componente = "<label ".$datos['nameid']." class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                            <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                                <input type='".$datos['tipo']."' id='".$datos['nameid']."' class='filestyle' data-placeholder='".$datos['valor']."' data-buttonText='Seleccionar ".$datos['titulo']."' data-buttonName='btn-primary' >
                            </div>";
                break;
        }
        return $componente;
    }

    private function newSelect($datos,$version,$get='',$crud='',$entidad='',$columnid='',$columnid2=''){
        $valDefecto = '';
        $valDefectoPadre = '';
        $componente = "";
        $script = '';
        $padre = '';
        $param2 = '';
        //POner Valores en caso de que tenga id o crud
        if (strlen($get)>1 && strlen($entidad) > 1) {
            $model = '\\Entidades\\'.$entidad;
            $mget = new $model($this->adapter);
            if (strlen($crud)>1) $dtget = $mget->getBy2($columnid,$get,$columnid2,$crud);
            else $dtget = $mget->getByTop1($columnid,$get);
            $valDefecto = ', "get" : "'.$dtget[$datos['bdd']].'"';
        }
        $componente .= "<label for = '".$datos['nameid']."' class='col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label'>".$datos['titulo']."</label>
                        <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12'>
                            <select class='form-control' id='".$datos['nameid']."' name='".$datos['nameid']."'></select>
                        </div>";
        //Validar si tiene padre
        if (strlen($datos['parametro']) > 0) {
            $padre = ', "parametro" : "'.$datos['parametro'].'"';
            
            if (strlen($datos['parametroValor2']) > 0) {
                $padre = '';
            }
            if (strlen($datos['parametro2']) > 0) {
                $param2 = '$("#'.$datos['parametro2'].'").html(response).fadeIn();';
            }
            //'.$valDefectoPadre.'
            self::$script .= '<script type="text/javascript">  $("#'.$datos['nameid'].'").change(function(){
                            var padre = $(this);
                            if($(this).val() != "")
                            {
                                $.ajax({
                                    data: {"modelo" : "'.$datos['parametro'].'" , "parametro": $(this).val()  },
                                    url: "'.CONTROLADOR_DEFECTO.'/getSelect", type:  "POST",
                                    success: function(response) { $("#'.$datos['parametroValor'].'").html(response).fadeIn(); '.$param2.' }
                                });
                            }
                        })</script>';
        }
        self::$script .= ' <script type="text/javascript"> $(document).ready(function() { $.ajax({ url: "'.CONTROLADOR_DEFECTO.'/getSelect",  data: {"modelo" : "'.$datos['modal'].'" '.$padre.' '.$valDefecto.'},type: "post", success: function(response) { $("#'.$datos['nameid'].'").html(response).fadeIn(); } }); }); </script>';
        
        return $componente;
    }
}
