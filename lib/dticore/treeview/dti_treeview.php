<?php

/*
 * Titulo: Creador de Tablas.
 * Author: Gabriel Reyes
 * Fecha: 10/05/2017
 * Version: 1.0.1
 *    */

class dti_treeview {
    
    public static $ctlVariables;
    
    public function __construct($datos) {
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("treeview");
            self::$ctlVariables = 0;
        }
        $script = '<script type="text/javascript">
                        $("#treeview").treeview({data:'. $this->arbol($datos).',enableLinks: true});
                    </script>';
        $variables::set("script",$script);
    }
    
    public function arbol($datos){
        if (\config\globalFunctions::es_bidimensional($datos)) {
            foreach ($datos as $row) {
                $tmp['id'] = $row['id'];
                $tmp['name'] = $row['nombre'];
                $tmp['text'] = $row['nombre'];
                $tmp['href'] = $row['href'];
                $tmp['padre_id'] = $row['padre_id'];            
                $data[] = $tmp; 
            }
        }
        else if (isset($datos["id"])) {
            $tmp['id'] = $datos['id'];
            $tmp['name'] = $datos['nombre'];
            $tmp['text'] = $row['nombre'];
            $tmp['href'] = $datos['href'];
            $tmp['padre_id'] = $datos['padre_id'];            
            $data[] = $tmp; 
        }
        
        foreach($data as $key => &$value) {
           $output[$value["id"]] = &$value;
        }
        
        foreach($data as $key => &$value) {
            if ($value["padre_id"] && isset($output[$value["padre_id"]])) {
                $output[$value["padre_id"]]["nodes"][] = &$value;
            }
        }
        
        foreach($data as $key => &$value) {
            if ($value["padre_id"] && isset($output[$value["padre_id"]])) {
                unset($data[$key]);
            }
        }
        
        return json_encode($data);
        
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
    }
}
