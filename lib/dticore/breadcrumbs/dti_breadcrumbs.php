<?php

/*
 * Titulo: Creador de Breadcrumbs.
 * Author: Gabriel Reyes
 * Fecha: 05/09/2017
 * Version: 1.0.1
 *    */

class dti_breadcrumbs {
    
    private static $breadcrumbs,$ctlVariables;
    
    public function __construct() {
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("breadcrumbs");
            self::$ctlVariables = 0;
        }
    }
    
    public function setBreadcrumbs($array,$color){
        self::$breadcrumbs = '<div class="btn-group btn-breadcrumb">'
                . '<a href="'.CONTROLADOR_DEFECTO.'/'.ACCION_DEFECTO.'" class="btn btn-'.$color.'"><i class="glyphicon glyphicon-home"></i></a>';
        foreach ($array as $key => $value) {
            switch ($key) {
                case '0':
                    self::$breadcrumbs .= '<a href="'.$value['nivel1'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
                case '1':
                    self::$breadcrumbs .= '<a href="'.$value['nivel2'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
                case '2':
                    self::$breadcrumbs .= '<a href="'.$value['nivel3'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
                case '3':
                    self::$breadcrumbs .= '<a href="'.$value['nivel4'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
                case '4':
                    self::$breadcrumbs .= '<a href="'.$value['nivel5'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
                case '5':
                    self::$breadcrumbs .= '<a href="'.$value['nivel6'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
                case '6':
                    self::$breadcrumbs .= '<a href="'.$value['nivel7'].'" class="btn btn-'.$value['color'].'">'.$value['nombre'].'</a>';
                    break;
            }
        }
        self::$breadcrumbs .='</div>';
    }
    
    public function getBreadcrumbs(){
        return self::$breadcrumbs;
    }
    
}