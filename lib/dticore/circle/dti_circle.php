<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_circle {

    private static $circle,$circulos,$ctlVariables;

    function setCircle($url,$icono,$descripcion){
        self::$circulos[] = array(
            "url"=>$url,
            "icono"=>$icono,
            "descripcion"=>$descripcion,
        );
    }

    public function __construct() {
        self::$circle = "<div class='row'><section class='section_0'>";
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("circle");
            self::$ctlVariables = 0;
        }
    }

    private function buildcircle(){
        foreach (self::$circulos as $cir) {
            self::$circle .= "<div class='col-lg-3 col-md-3 col-sm-3'>
                                <div class='circle circle3'>
                                    <a href='".$cir["url"]."'><h2><i class='fa ".$cir["icono"]."'></i><p>".$cir["descripcion"]."</p></h2></a>
                                </div>
                              </div>";
        }
    }

    public function getCircle(){
        $this->buildcircle();
        self::$circle .= "</section></div>";
        return self::$circle;
    }
}
