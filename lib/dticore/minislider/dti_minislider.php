<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_minislider {

    private static $minislider,$ctlVariables;

    function setMinislider(){
        self::$minislider = '<div class="container">
                                <div class="row">
                                    <div class="span12">
                                        <div class="well">
                                            <div id="myCarousel" class="carousel fdi-Carousel slide">
                                             <!-- Carousel items -->
                                                <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                                                    <div class="carousel-inner onebyone-carosel">
                                                        <div class="item active">
                                                            <div class="col-md-4">
                                                                <a href="#"><img src="public/images/img250x250.png" class="img-responsive center-block"></a>
                                                                <div class="text-center">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="col-md-4">
                                                                <a href="#"><img src="public/images/img250x250.png" class="img-responsive center-block"></a>
                                                                <div class="text-center">2</div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="col-md-4">
                                                                <a href="#"><img src="public/images/img250x250.png" class="img-responsive center-block"></a>
                                                                <div class="text-center">3</div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="col-md-4">
                                                                <a href="#"><img src="public/images/img250x250.png" class="img-responsive center-block"></a>
                                                                <div class="text-center">4</div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="col-md-4">
                                                                <a href="#"><img src="public/images/img250x250.png" class="img-responsive center-block"></a>
                                                                <div class="text-center">5</div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="col-md-4">
                                                                <a href="#"><img src="public/images/img250x250.png" class="img-responsive center-block"></a>
                                                                <div class="text-center">6</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>
                                                    <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>
                                                </div>
                                                <!--/carousel-inner-->
                                            </div><!--/myCarousel-->
                                        </div><!--/well-->
                                    </div>
                                </div>
                            </div>';
    }

    public function __construct() {
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("minislider");
            self::$ctlVariables = 0;
        }
    }

    public function getMinislider(){
        return self::$minislider;
    }
}