<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *
 */
class dti_step {

    private static $ctlVariables,$step,$maestro,$detalle,$activo;

    public function __construct() {
        self::$step = '';
        self::$maestro = '';
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("step");
            self::$ctlVariables = 0;
        }
    }
    
    /**
     *
     * @param array $dt id,tip,icono,contenido,siguiente,anterior,saltar,fin,onclic
     */
    public function setStep($dt){
        $botones = '';
        if (isset($dt['siguiente'])) $botones .= '<li><button type="button" class="btn btn-primary next-step">Siguiente</button></li>';
        if (isset($dt['anterior'])) $botones .= '<li><button type="button" class="btn btn-default prev-step">Anterior</button></li>';
        if (isset($dt['saltar'])) $botones .= '<li><button type="button" class="btn btn-default next-step">Saltar</button></li>';
        if (isset($dt['fin'])) $botones .= '<li><button type="button" class="btn btn-primary" onclick="location.href=\''.$dt['onclic'].'\'">Finalizar</button></li>';
            
        if ($dt['activo']) {
            
            self::$maestro .= '<li role="presentation" class="active">
                                    <a href="#'.$dt['id'].'" data-toggle="tab" aria-controls="'.$dt['id'].'" role="tab" title="'.$dt['tip'].'">
                                        <span class="round-tab">
                                            <i class="'.$dt['icono'].'"></i>
                                        </span>
                                    </a>
                                </li>';
            self::$detalle .= '<div class="tab-pane active" role="tabpanel" id="'.$dt['id'].'">
                                '.$dt['contenido'].'
                                <ul class="list-inline pull-right">
                                    '.$botones.'
                                </ul>
                            </div>';
        }else{
            self::$maestro .= '<li role="presentation" class="disabled">
                                    <a href="#'.$dt['id'].'" data-toggle="tab" aria-controls="'.$dt['id'].'" role="tab" title="'.$dt['tip'].'">
                                        <span class="round-tab">
                                            <i class="'.$dt['icono'].'"></i>
                                        </span>
                                    </a>
                                </li>';
            self::$detalle .= '<div class="tab-pane" role="tabpanel" id="'.$dt['id'].'">
                                '.$dt['contenido'].'
                                <ul class="list-inline pull-right">
                                    '.$botones.'
                                </ul>
                            </div>';
        }
    }

    public function getStep(){
        self::$step .= '<div class="wizard">
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">';
        //Agregar el Maestro
        self::$step .= self::$maestro;

        self::$step .= '</ul>
                       </div>

                       <form role="form">
                           <div class="tab-content">';
        //Agregar el detalle
        self::$step .= self::$detalle;

        self::$step .= '         <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>';

        return self::$step;
    }
}