<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_box {

    private static $box,$ctlVariables;

    function setBox(){
        self::$box = '<div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="box">
                                    <div class="box-icon">
                                        <span class="fa fa-4x fa-html5"></span>
                                    </div>
                                    <div class="info">
                                        <h4 class="text-center">Title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                                        <a href="" class="btn">Link</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="box">
                                    <div class="box-icon">
                                        <span class="fa fa-4x fa-css3"></span>
                                    </div>
                                    <div class="info">
                                        <h4 class="text-center">Title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!</p>
                                        <a href="" class="btn">Link</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>';
    }

    public function __construct() {
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("box");
            self::$ctlVariables = 0;
        }
    }

    public function getBox(){
        return self::$box;
    }
}