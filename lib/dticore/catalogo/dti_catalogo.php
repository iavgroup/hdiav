<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_catalogo {

    private static $catalogo,$ctlVariables;

    function setCatalogo($controllers,$datosCatalogo){
        self::$catalogo = '<div id="loaderCatalogo" style="position: absolute;text-align: center;top: 55px;width: 100%;display:none;"></div>';
        self::$catalogo .= '<div class="row">';
        if (\config\globalFunctions::es_bidimensional($datosCatalogo)) {
            foreach ($datosCatalogo as $datos) {
                self::$catalogo .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="col-item">
                                            <div class="photo">
                                                <img src="public/images/img350x260.png" class="img-responsive" alt="a" />
                                            </div>
                                            <div class="info">
                                                <div class="row">
                                                    <div class="price col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <h4>'.$datos["id"].' </h4>
                                                        <h5>'.$datos["producto"].' </h5>
                                                        <h5 class="price-text-color">$'.$datos["precio"].'</h5>
                                                        <input type="number" id="txt'.$datos["id"].'" value="0" style="width:50px;" /></br>
                                                        Stock actual: <label id="lbl'.$datos["id"].'" value="'.$datos["cantidad"].'">'.$datos["cantidad"].' </label>
                                                    </div>
                                                    <!--<div class="rating hidden-sm col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <!--<i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                                                        </i><i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                                                        </i><i class="fa fa-star"></i>
                                                    </div>-->
                                                </div>
                                                <div class="separator clear-left">
                                                    <p class="btn-add">
                                                        <button type="button" class="btn btn-default hidden-sm" onclick="goAgregarProducto(\''.$controllers.'\',\''.$datos["id"].'\')"><i class="fa fa-shopping-cart"></i> Agregar</button></p>
                                                    <!--<p class="btn-details">
                                                        <button type="button" class="btn btn-default hidden-sm"><i class="fa fa-list"></i> Más Detalles</button></p>-->
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                  </div>';
            }
        }
        else{
            self::$catalogo .= '<div class="col-sm-3">
                                    <div class="col-item">
                                        <div class="photo">
                                            <img src="public/images/img350x260.png" class="img-responsive" alt="a" />
                                        </div>
                                         <div class="info">
                                                <div class="row">
                                                    <div class="price col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <h4>'.$datosCatalogo["id"].' </h4>
                                                        <h5>'.$datosCatalogo["producto"].' </h5>
                                                        <h5 class="price-text-color">$'.$datosCatalogo["precio"].'</h5>
                                                        <input type="number" id="txt'.$datosCatalogo["id"].'" value="0" style="width:50px;" /></br>
                                                        Stock actual: <label id="lbl'.$datosCatalogo["id"].'" value="'.$datosCatalogo["cantidad"].'">'.$datosCatalogo["cantidad"].' </label>
                                                    </div>
                                                    <!--<div class="rating hidden-sm col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <!--<i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                                                        </i><i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                                                        </i><i class="fa fa-star"></i>
                                                    </div>-->
                                                </div>
                                                <div class="separator clear-left">
                                                    <p class="btn-add">
                                                        <button type="button" class="btn btn-default hidden-sm" onclick="goAgregarProducto(\''.$controllers.'\',\''.$datosCatalogo["id"].'\')"><i class="fa fa-shopping-cart"></i> Agregar</button></p>
                                                    <!--<p class="btn-details">
                                                        <button type="button" class="btn btn-default hidden-sm"><i class="fa fa-list"></i> Más Detalles</button></p>-->
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                  </div>';
        }
        self::$catalogo .= '</div>';
    }

    public function __construct() {
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("catalogo");
            self::$ctlVariables = 0;
        }
    }

    public function getCatalogo(){
        return self::$catalogo;
    }
}