<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_slider {

    private static $id,$slider,$ctlVariables,$activo,$maestro,$detalle,$contador;

    function setSlider($imagen,$titulo,$descripcion){
        if (self::$activo == '') {
            self::$maestro .= '<li data-target="#'.self::$id.'" data-slide-to="'.self::$contador.'" class="active"></li>';
            self::$detalle .= '<div class="item active"><img src="public/uploads/'.$imagen.'" alt="..." style="width:100%;"></div>';
            self::$activo = 'completo';
        }else{
            self::$maestro .= '<li data-target="#'.self::$id.'" data-slide-to="'.self::$contador.'"></li>';
            self::$detalle .= '<div class="item"><img src="public/uploads/'.$imagen.'" alt="..." style="width:100%;"></div>';
        }
        self::$contador++;
    }

    public function __construct($idSlider) {
        //Limpiamos las variables para volver a llamar
        self::$slider = '';
        self::$activo = '';
        self::$maestro = '';
        self::$detalle = '';
        self::$contador = 0;
        self::$id = $idSlider;
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("slider");
            self::$ctlVariables = 0;
        }
    }

    public function getSlider(){
        self::$slider = '<div id="'.self::$id.'" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">'.self::$maestro.'</ol><div class="carousel-inner">'
                . ''.self::$detalle.'</div><a class="left carousel-control" href="#'.self::$id.'" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Anterior</span>
                      </a>
                      <a class="right carousel-control" href="#'.self::$id.'" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Siguiente</span>
                      </a>
                    </div>';
        return self::$slider;
    }
}
