<?php

/**
 * Funciones Globales DtiWare
 *
 * @category   Bot Facebook
 * @copyright  Copyright (c) DtiWare
 * @author	   Fabriel Reyes
 * @version    2.0.0
 */

//Manejador de Texto
class dti_botFacebook {
	//Manejador de Texto
    public function getTexto($messageText) {
    	if (stristr(strtolower($messageText), "hola"))
		{
			$respuesta = "Hola, en que te podemos ayudar?";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Imagen Internacional Cosmetología, Cosmiatria y Maquillaje Profesional. \n\n Pone a su disposicion cursos avalados a nivel nacional. \n\n Duracion del Curso: 12 Meses, 1 dia a la semana o via online \n\n Horarios: \n Matutino: 09:00 - 13:00 \n Vespertino: 14:00 - 18:00 \n Nocturno: 18:00 - 21:00 \n\n Requisitos: \n -Copia de Cedula y Papeleta de Votacion a Color. \n -2 Fotografias a Color \n\n Direccion: Quito, Toledo N24-661 y Av. La Coruña, Sector Plaza Artigas. \n Telefono: (02)2222612 / 0991354975.";
			return $respuesta;
		}
		if ((stristr(strtolower($messageText), "precios") || stristr(strtolower($messageText), "costo")) && (!stristr(strtolower($messageText), "pago") || !stristr(strtolower($messageText), "pagar")))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Precios: \n Matricula: 60 Dolares \n Pension: 75 Dolares.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "duracion") && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Duracion: \n 12 Meses, 1 dia a la semana o via online (en via online el ultimo domingo de cada mes tiene que realizar la practica presencial) \n\n Horarios: \n Matutino: 09:00 - 13:00 \n Vespertino: 14:00 - 18:00 \n Nocturno: 18:00 - 21:00";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "horario") && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Horarios: \n Matutino: 09:00 - 13:00 \n Vespertino: 14:00 - 18:00 \n Nocturno: 18:00 - 21:00 \n , Usted escoge un dia semana de Lunes a Sabado.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "semana") && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Usted puede escoger el dia que desee entre el Lunes y el Sabado en cualquiera de nuestros horarios: \n Matutino: 09:00 - 13:00 \n Vespertino: 14:00 - 18:00 \n Nocturno: 18:00 - 21:00.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "lugar") && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Quito, Toledo N24-661 Y Av. La Coruña Sector Plaza Artigas. Tlf: 02 2222612 / 0991354975 \n Ambato, Av de los Guaytambos 06-87 y La Delicia junto a restaurante ALIS, Ficoa. Tlf 03 2421753 / 0991354975";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "telefonos") && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Quito, Toledo N24-661 Y Av. La Coruña Sector Plaza Artigas. Tlf: 02 2222612 / 0991354975 \n Ambato, Av de los Guaytambos 06-87 y La Delicia junto a restaurante ALIS, Ficoa. Tlf 03 2421753 / 0991354975";
			return $respuesta;
		}
		if ((stristr(strtolower($messageText), "sitio") || stristr(strtolower($messageText), "ubica") || stristr(strtolower($messageText), "direccion")) && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Quito, Toledo N24-661 Y Av. La Coruña Sector Plaza Artigas. Tlf: 02 2222612 / 0991354975 \n Ambato, Av de los Guaytambos 06-87 y La Delicia junto a restaurante ALIS, Ficoa. Tlf 03 2421753 / 0991354975";
			return $respuesta;
		}
		if ((stristr(strtolower($messageText), "precios") || stristr(strtolower($messageText), "costo")) && (stristr(strtolower($messageText), "pago")) || stristr(strtolower($messageText), "pagar"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Puede acercarse a cualquiera de nuestras surcursales o realizar un deposito. \n\n Quito, Toledo N24-661 Y Av. La Coruña Sector Plaza Artigas. Tlf: 02 2222612 / 0991354975 \n Ambato, Av de los Guaytambos 06-87 y La Delicia junto a restaurante ALIS, Ficoa. Tlf 03 2421753 / 0991354975";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "deposito"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Puede realizar los depositos en: \n Banco del Pichincha: Cuenta de Ahorros a nombre del Centro de Capacitacion Imagen Internacional #3398396100 con Cedula #1801851716.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "certificado"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Se le entrega un Certificado avalado por el SETEC (Secretaría Técnica de Capacitación Profesional). Con el cual puede ponerse su propio negocio.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "fecha"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Cada semana se inicia un nuevo curso. Matriculate y escoge el dia y horario que más se acople a tus necesidades.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "dias") && stristr(strtolower($messageText), "curso"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Usted puede escoger el dia que desee entre el Lunes y el Sabado en cualquiera de nuestros horarios: \n Matutino: 09:00 - 13:00 \n Vespertino: 14:00 - 18:00 \n Nocturno: 18:00 - 21:00.";
			return $respuesta;
		}
		if (stristr(strtolower($messageText), "forma") && stristr(strtolower($messageText), "pago"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " puede realizar en efectivo, deposito o transferenciar.";
			return $respuesta;
		}
		if ((stristr(strtolower($messageText), "requisito") && stristr(strtolower($messageText), "matricula") && !stristr(strtolower($messageText), "informacion"))
			|| (stristr(strtolower($messageText), "requisito") && stristr(strtolower($messageText), "inscripci") && !stristr(strtolower($messageText), "informacion")))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " Requisitos para la inscripcion: \n\n Cursos: \n 
		    -Copia de Cédula y Papeleta de Votación a color.
		    -Pasaporte para Extranjeros.
		    -2 Fotografías a color para el finar entrega de Carnet de Cosmetologa.\n\n Curso Cosnetol: \n 
		    -Requisito ser bachiller.
		    -Copia de Cédula y Papeleta de Votación a color.
		    -Pasaporte para Extranjeros.
		    -2 Fotografías a color.";
		    return $respuesta;
		}
		if (stristr(strtolower($messageText), "incluye") && stristr(strtolower($messageText), "material") && !stristr(strtolower($messageText), "informacion"))
		{
			//Saludo
			$hora = (localtime(time(),true));
			if ($hora["tm_hour"] >= 0) { $respuesta = "Buenos Dias,"; }
			if ($hora["tm_hour"] >= 12) { $respuesta = "Buenos Tardes,"; }
			if ($hora["tm_hour"] >= 18) { $respuesta = "Buenos Noches,"; }
			$respuesta .= " No incluye materiales, usted tiene que pagar los materiales aparte.";
			return $respuesta;
		}
		return $respuesta = "Lo Siento no entiendo tu pregunta.";
    }
    //Quita la tildes
    public function quitarTildes($palabra){
    	//Variables
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		//Quitar Tildes
		$palabra= str_replace($no_permitidas, $permitidas ,$palabra);
		return $palabra;
    }
    //Envia los mensajes
    public function setMensaje($accessToken,$response){
    	// <<<< Envia el mensaje >>>>
		$ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token='.$accessToken);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_exec($ch);
		curl_close($ch);
    }

    public function setSaludoInicial($accessToken,$response){
    	// <<<< Envia el mensaje >>>>
		$ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token='.$accessToken);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_exec($ch);
		curl_close($ch);
    }

    public function getImagenes(){
    	//imagen
		if($messageText == "imagen69"){
		     $answer = ["attachment"=>[
		      "type"=>"image",
		      "payload"=>[
		        "url"=>"https://www.cloudways.com/blog/wp-content/uploads/Migrating-Your-Symfony-Website-To-Cloudways-Banner.jpg"
		        ]
		      ]
		    ];
		     $response = [
		    'recipient' => [ 'id' => $senderId ],
		    'message' => $answer 
		];}
    }

    public function getBlogs($messageText){
    	//blog
		if($messageText == "blog69"){
		     $answer = ["attachment"=>[
		      "type"=>"template",
		      "payload"=>[
		        "template_type"=>"generic",
		        "elements"=>[
		          [
		            "title"=>"Welcome to Peter\'s Hats",
		            "item_url"=>"https://www.cloudways.com/blog/migrate-symfony-from-cpanel-to-cloud-hosting/",
		            "image_url"=>"https://www.cloudways.com/blog/wp-content/uploads/Migrating-Your-Symfony-Website-To-Cloudways-Banner.jpg",
		            "subtitle"=>"We\'ve got the right hat for everyone.",
		            "buttons"=>[
		              [
		                "type"=>"web_url",
		                "url"=>"https://petersfancybrownhats.com",
		                "title"=>"View Website"
		              ],
		              [
		                "type"=>"postback",
		                "title"=>"Start Chatting",
		                "payload"=>"DEVELOPER_DEFINED_PAYLOAD"
		              ]              
		            ]
		          ]
		        ]
		      ]
		    ]];
		    return $answer;
		  }
    }

    public function getSanIsidro($messageText){
    	//blog
			$answer = ["attachment"=>[
			"type"=>"template",
			"payload"=>[
			"template_type"=>"generic",
			"elements"=>[
				[
				"title"=>"Bienvenido a Centro Turistico San Isidro",
				"image_url"=>"https://scontent.fatf1-1.fna.fbcdn.net/v/t1.0-9/1625705_230539990468587_711319000_n.jpg?oh=e5d511951504474520407fc910e41475&oe=5A353529",
				"subtitle"=>"Nosotros estamos gustosos en poder ayudarte, Realiza tu cotización.",
				"buttons"=>[
					[
					"type"=>"postback",
					"title"=>"Empezar Cotización",
					"payload"=>"COTIZAR"
					],
					[
					"type"=>"postback",
					"title"=>"Cotización Total",
					"payload"=>"DEFECTO"
					]
				]
				]
			]
			]
		]];	
		return $answer;	  
    }

    public function getCotizar($messageText){
    	//blog
			$answer = ["attachment"=>[
			"type"=>"template",
			"payload"=>[
			"template_type"=>"button",
			"text"=>"Que Deseas Conocer?",
			"buttons"=>[
					[
					"type"=>"postback",
					"title"=>"Regresar",
					"payload"=>"Regresar"
					],
					[
					"type"=>"postback",
					"title"=>"Manteleria",
					"payload"=>"Manteleria"
					],
					[
					"type"=>"postback",
					"title"=>"Vajilla",
					"payload"=>"Vajilla"
					]
				]
			]
		]];	
		return $answer;	  
    }

    public function getManteleria($messageText){
    	//blog
			$answer = ["attachment"=>[
			"type"=>"template",
			"payload"=>[
			"template_type"=>"list",
			"top_element_style"=>"compact",
			"elements"=>[
				[
					"title"=>"Cubre Sillas",
					"image_url"=>"http://programascipreses.dtiware.com/images/cubresillas.jpg",
					"subtitle"=>"Cada cubre sillas => \$0.45",
					"buttons"=>[
						[
							"type"=>"postback",
							"title"=>"Agregar",
							"payload"=>"Manteleria_CubreSillas"
						]
					]
				],
				[
					"title"=>"Cubre Mesas",
					"image_url"=>"http://programascipreses.dtiware.com/images/cubresillas.jpg",
					"subtitle"=>"Cada cubre mesas => \$0.45",
					"buttons"=>[
						[
							"type"=>"postback",
							"title"=>"Agregar",
							"payload"=>"Manteleria_CubreMesas"
						]
					]
				],
				[
					"title"=>"Regresar",
					"buttons"=>[
						[
							"type"=>"postback",
							"title"=>"Regresar",
							"payload"=>"cotizar"
						]
					]
				],
			]
			]
		]];	
		return $answer;	  
    }
}