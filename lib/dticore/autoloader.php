<?php

//Registra las clases necesarias para la funcion de cada Controlador
spl_autoload_register(function($clase){
    //Agrega Configuracion de Formularios
    $ruta = "lib/dticore/form/".str_replace("\\", "/", $clase).".php";
    if (is_readable($ruta)) {
        require_once $ruta;
    }else{
        //Agrega Configuracion de Boxquick
        $ruta = "lib/dticore/boxquick/".str_replace("\\", "/", $clase).".php";
        if (is_readable($ruta)) {
            require_once $ruta;
        }else{
            //Agrega Configuracion de Core
            $ruta = "lib/dticore/core/".str_replace("\\", "/", $clase).".php";
            if (is_readable($ruta)) {
                require_once $ruta;
            }else{
                //Agrega Configuracion de Tabla
                $ruta = "lib/dticore/table/".str_replace("\\", "/", $clase).".php";
                if (is_readable($ruta)) {
                    require_once $ruta;
                }else{
                    //Agrega Configuracion de Circle
                    $ruta = "lib/dticore/circle/".str_replace("\\", "/", $clase).".php";
                    if (is_readable($ruta)) {
                        require_once $ruta;
                    }else{
                        //Agrega Configuracion de Circle
                        $ruta = "lib/dticore/builder/".str_replace("\\", "/", $clase).".php";
                        if (is_readable($ruta)) {
                            require_once $ruta;
                        }else{
                            //Agrega Configuracion de Circle
                            $ruta = "lib/dticore/treeview/".str_replace("\\", "/", $clase).".php";
                            if (is_readable($ruta)) {
                                require_once $ruta;
                            }else{
                                //Agrega Configuracion de Circle
                                $ruta = "lib/dticore/components/".str_replace("\\", "/", $clase).".php";
                                if (is_readable($ruta)) {
                                    require_once $ruta;
                                }else{
                                    $ruta = "lib/dticore/panel/".str_replace("\\", "/", $clase).".php";
                                    if (is_readable($ruta)) {
                                        require_once $ruta;
                                    }
                                    else{
                                        $ruta = "lib/dticore/box/".str_replace("\\", "/", $clase).".php";
                                        if (is_readable($ruta)) {
                                            require_once $ruta;
                                        }else{
                                            $ruta = "lib/dticore/minislider/".str_replace("\\", "/", $clase).".php";
                                            if (is_readable($ruta)) {
                                                require_once $ruta;
                                            }else{
                                                $ruta = "lib/dticore/catalogo/".str_replace("\\", "/", $clase).".php";
                                                if (is_readable($ruta)) {
                                                    require_once $ruta;
                                                }else{
                                                    $ruta = "lib/dticore/subirdoc/".str_replace("\\", "/", $clase).".php";
                                                    if (is_readable($ruta)) {
                                                        require_once $ruta;
                                                    }else{
                                                        $ruta = "lib/dticore/login/".str_replace("\\", "/", $clase).".php";
                                                        if (is_readable($ruta)) {
                                                            require_once $ruta;
                                                        }else{
                                                            $ruta = "lib/dticore/botFacebook/".str_replace("\\", "/", $clase).".php";
                                                            if (is_readable($ruta)) {
                                                                require_once $ruta;
                                                            }else{
                                                                $ruta = "lib/dticore/breadcrumbs/".str_replace("\\", "/", $clase).".php";
                                                                if (is_readable($ruta)) {
                                                                    require_once $ruta;
                                                                }else{
                                                                    $ruta = "lib/dticore/grafico/".str_replace("\\", "/", $clase).".php";
                                                                    if (is_readable($ruta)) {
                                                                        require_once $ruta;
                                                                    }else{
                                                                        $ruta = "lib/dticore/slider/".str_replace("\\", "/", $clase).".php";
                                                                        if (is_readable($ruta)) {
                                                                            require_once $ruta;
                                                                        }else{
                                                                            $ruta = "lib/dticore/step/".str_replace("\\", "/", $clase).".php";
                                                                            if (is_readable($ruta)) {
                                                                                require_once $ruta;
                                                                            }else{
                                                                                $ruta = "lib/dticore/prueba/".str_replace("\\", "/", $clase).".php";
                                                                                if (is_readable($ruta)) {
                                                                                    require_once $ruta;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
});
