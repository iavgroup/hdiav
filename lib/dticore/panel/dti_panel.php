<?php

/*
 * Titulo: Creador de Formularios.
 * Author: Gabriel Reyes
 * Fecha: 05/05/2017
 * Version: 1.0.1
 *    */

class dti_panel {

    private static $panel,$ctlVariables,$cabecera,$detalle,$activa;

    /**
     * Funcion para crear paneles
     * 
     * @param array $dtcolumna page,icono,titulo
     * @param string $dtdetalle html detalle
     */
    function setPanel($dtcolumna,$dtdetalle){
        if (self::$activa == 0) {
            self::$cabecera .= '<li role="presentation" class="active"><a href="#'.$dtcolumna['page'].'" aria-controls="'.$dtcolumna['page'].'" role="tab" data-toggle="tab"><i class="fa '.$dtcolumna['icono'].'"></i>  <span>'.$dtcolumna['titulo'].'</span></a></li>';
            self::$detalle .= ' <div role="tabpanel" class="tab-pane active" id="'.$dtcolumna['page'].'">'.$dtdetalle.'</div>';
            self::$activa = 1;
        }else{
            self::$cabecera .= '<li role="presentation"><a href="#'.$dtcolumna['page'].'" aria-controls="'.$dtcolumna['page'].'" role="tab" data-toggle="tab"><i class="fa '.$dtcolumna['icono'].'"></i>  <span>'.$dtcolumna['titulo'].'</span></a></li>';
            self::$detalle .= '<div role="tabpanel" class="tab-pane" id="'.$dtcolumna['page'].'">'.$dtdetalle.'</div>';
        }
    }

    public function __construct() {
        //Limpiamos las variables para volver a llamar
        self::$cabecera = '';
        self::$detalle = '';
        self::$panel = '';
        self::$activa = 0;
        //Cargas Css/Js/Script Obligatorios
        if (!isset(self::$ctlVariables)) {
            $variables = new \dti_core("panel");
            self::$ctlVariables = 0;
            self::$activa = 0;
        }
    }

    public function getPanel(){
        self::$panel .= '<div class="container">
                            <div class="row">
                              <div class="col-md-12">
                                <!-- Nav tabs -->
                                <div class="card">
                                    <ul class="nav nav-tabs" role="tablist">';
        
        //Cabecera
        self::$panel .= self::$cabecera;
        
        self::$panel .= '</ul>
                            <!-- Tab panes -->
                        <div class="tab-content">';
        
        //Detalle
        self::$panel .= self::$detalle;
        
        self::$panel .= '         </div>
                                </div>
                              </div>
                            </div>
                          </div>';
        
        return self::$panel;
    }
}
