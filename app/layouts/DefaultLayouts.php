<?php

namespace layouts;
/**
 * @class: Creacion de Codigo
 * @autor: Gabriel Reyes
 * @fecha: 30/03/2017
 * @version: 0.0.1
 */

class DefaultLayouts {

    private $website;
    private $conectar;
    private $adapter;
    public static $css = "";
    public static $js = "";
    public static $script = "";
    public static $ctlVariables;

    public function __construct($website) {
        $this->website = $website;
        //Conexion
        $this->conectar = new \core\Conectar();
        $this->adapter= $this->conectar->conexion();
        if (!isset(self::$ctlVariables)) {
            //Cargas Css/Js/Script Obligatorios
            $variables = new \dti_core($this->website["template_portal"]);
            self::$ctlVariables = 0;
        }
    }

    /*
     * Function: Crea todo el codigo que va dentro de los id de los div
     */
    public function renderizar($datos){
        //Convertir en variables los datos enviados.
        foreach ($datos as $id_assoc => $valor){
            ${$id_assoc}=$valor;
        }
        /*
         * DECLARACION DE VARIABLES
         */
        $section_contents_tpl['dti_area_content'] = "";
        /*
         * TEMPLATE
         */
        $template = $this->templateAction($this->website["template_portal"]);
        \dti_core::set("css", $this->cssAction($this->website["template_portal"]));
        /*
         * MANEJO DE SECCIONES
         */
        foreach ($section as $key => $value) {
            switch ($key) {
                case 'titulo':
                    $section_contents_tpl["dti_area_content"] .= "<section class='row'>". $this->tituloAction($value) . "</section>";
                    break;
                case 'panel':
                    $section_contents_tpl["dti_area_content"] .= "<section class='row'>". $value . "</section>";
                    break;
                case 'manual':
                    $section_contents_tpl["dti_area_content"] .= "<section class='row'>". $value . "</section>";
                    break;
            }
        }

        //Insertar en los id de los div el codigo que necesitamos
        $render = \config\globalFunctions::renderizarTotal($template,$section_contents_tpl);
        return $render;
    }
    
    public function templateAction($template){
        /********
	 * template render
	 */
        $render = "";
        $webtemplate = fopen("public/template/".$template."/template.php", "r");
        if($webtemplate)
        {
                //Output a line of the file until the end is reached
                while(!feof($webtemplate))
                {
                        $render.= fgets($webtemplate);
                }
                fclose($webtemplate);
        }
        return $render;
    }
    
    public function cssAction($componente){
        switch ($componente) {
            case "template1":
                $cssAction = "<link href='public/template/template1/style.css' rel='stylesheet' type='text/css'/>";
                break;
            case "template2":
                $cssAction = "<link href='public/template/template2/style.css' rel='stylesheet' type='text/css'/>";
                break;
            case "template3":
                $cssAction = "<link href='public/template/template3/style.css' rel='stylesheet' type='text/css'/>";
                break;
            case "template4":
                $cssAction = "<link href='public/template/template4/style.css' rel='stylesheet' type='text/css'/>";
                break;
            case "template5":
                $cssAction = "<link href='public/template/template5/style.css' rel='stylesheet' type='text/css'/>";
                break;
            case "template6":
                $cssAction = "<link href='public/template/template6/style.css' rel='stylesheet' type='text/css'/>";
                break;
        }
        return $cssAction;
    }
}