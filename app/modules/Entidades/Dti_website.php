<?php

namespace Entidades;

class Dti_website extends \core\EntidadBase {
    private $id, $languageid, $templateid, $nombre, $descripcion, $telefono, $keywords, 
            $website_url, $logo, $icon, $info_email, $copyright,
            $smtp_hostname, $smtp_port, $smtp_username, $smtp_password,$iva;
    
    public function __construct($adapter) {
        $table="dti_website";
        parent::__construct($table,$adapter);
    }
    function getTelefono() {
        return $this->telefono;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function getId() {
        return $this->id;
    }

    function getLanguageid() {
        return $this->languageid;
    }

    function getTemplateid() {
        return $this->templateid;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getKeywords() {
        return $this->keywords;
    }

    function getWebsite_url() {
        return $this->website_url;
    }

    function getLogo() {
        return $this->logo;
    }

    function getIcon() {
        return $this->icon;
    }

    function getInfo_email() {
        return $this->info_email;
    }

    function getCopyright() {
        return $this->copyright;
    }

    function getSmtp_hostname() {
        return $this->smtp_hostname;
    }

    function getSmtp_port() {
        return $this->smtp_port;
    }

    function getSmtp_username() {
        return $this->smtp_username;
    }

    function getSmtp_password() {
        return $this->smtp_password;
    }
    
    function getIva() {
        return $this->iva;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLanguageid($languageid) {
        $this->languageid = $languageid;
    }

    function setTemplateid($templateid) {
        $this->templateid = $templateid;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    function setWebsite_url($website_url) {
        $this->website_url = $website_url;
    }

    function setLogo($logo) {
        $this->logo = $logo;
    }

    function setIcon($icon) {
        $this->icon = $icon;
    }

    function setInfo_email($info_email) {
        $this->info_email = $info_email;
    }

    function setCopyright($copyright) {
        $this->copyright = $copyright;
    }

    function setSmtp_hostname($smtp_hostname) {
        $this->smtp_hostname = $smtp_hostname;
    }

    function setSmtp_port($smtp_port) {
        $this->smtp_port = $smtp_port;
    }

    function setSmtp_username($smtp_username) {
        $this->smtp_username = $smtp_username;
    }

    function setSmtp_password($smtp_password) {
        $this->smtp_password = $smtp_password;
    }
    
    function setIva($iva) {
        $this->iva = $iva;
    }
    
    public function save(){
        $query="INSERT INTO dti_website(id,languageid,templateid,nombre,descripcion,telefono,keywords,website_url,logo,icon,info_email,copyright,smtp_hostname,smtp_port,smtp_username,smtp_password,iva)
                VALUES(NULL,
                '".$this->languageid."',
                '".$this->templateid."',
                '".$this->nombre."',
                '".$this->descripcion."',
                '".$this->telefono."',
                '".$this->keywords."',
                '".$this->website_url."',
                '".$this->logo."',
                '".$this->icon."',
                '".$this->info_email."',
                '".$this->copyright."',
                '".$this->smtp_hostname."',
                '".$this->smtp_port."',
                '".$this->smtp_username."',
                '".$this->smtp_password."',
                '".$this->iva."');";
        $save=$this->db()->query($query);
        $this->db()->error;
        return $save;
    }
}