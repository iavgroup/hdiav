<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Entidades;

/**
 * Description of Dti_usuario
 *
 * @author virtual
 */
class Hd_tusuario {
    //put your code here
       private $id, $usuario, $clave, $nombre, $apellido, $correo, $activo, 
            $fecha_creacion, $empresa, $idrol;
       
       public function __construct($adapter) {
        $table="hd_tusario";
        parent::__construct($table,$adapter);
        }
        
        function getId() {
            return $this->id;
        }

        function getUsuario() {
            return $this->usuario;
        }

        function getClave() {
            return $this->clave;
        }

        function getNombre() {
            return $this->nombre;
        }

        function getApellido() {
            return $this->apellido;
        }

        function getCorreo() {
            return $this->correo;
        }

        function getActivo() {
            return $this->activo;
        }

        function getFecha_creacion() {
            return $this->fecha_creacion;
        }

        function getEmpresa() {
            return $this->empresa;
        }

        function getIdrol() {
            return $this->idrol;
        }
        
        public function insertar(){
        $query="INSERT INTO hd_tusuario(id,usuario,clave,nombre,apellido,correo,activo,fecha_creacion,empresa,idrol)
                VALUES(NULL,
                '".$this->usuario."',
                '".$this->clave."',
                '".$this->nombre."',
                '".$this->apellido."',
                '".$this->correo."',
                '".$this->activo."',
                '".$this->fecha_creacion."',
                '".$this->empresa."',
                '".$this->idrol."');";
        $save=$this->db()->query($query);
        $this->db()->error;
        return $save;
    }

         
}
