<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

/**
 * Description of JfreireControllers
 *
 * @author virtual
 */
class JfreireControllers extends \core\ControladorBase {
    /**
     * Declaracion de Variables
     */
    
    public $conectar;
    public $adapter;
    public $website;
    public $layout;

    public function __construct($tipo) {
        //Inicializamos al padre
        parent::__construct($tipo);
        //Conexion a la base de datos
        $this->conectar = new \core\Conectar();
        $this->adapter= $this->conectar->conexion();
        //Traemos los datos del portal configurados
        $this->website=new \Models\Dti_websiteModel($this->adapter);
        $this->website=$this->website->getWebsite();
        //Cargamos el layout
        $this->layout = new \layouts\DefaultLayouts($this->website);
    }
    
    public function login(){
        //$login = new \Models\Hd_seguridadModel($this->adapter);
        //$dtlogin = $login->getLogin('adminasasas', 'admin');
//        foreach ($dtlogin as $fila) {
//            $resultado = $fila['MSG'];
//        }
        //print_r($dtlogin['MSG']);
        //echo 'Finalizado';
        
        $manual = '<section class="login-block">
            <div class="container">
            <div class="row">
                <div class="col-md-4 login-sec">
                    <h2 class="text-center">HELP DESK</h2>
                    <form class="login-form">
                        <div class="form-group">
                        <label for="exampleInputEmail1" class="text-uppercase">Email</label>
                        <input type="text" class="form-control" placeholder="">
                        
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1" class="text-uppercase">Contraseña</label>
                        <input type="password" class="form-control" placeholder="">
                      </div>
                      <div class="form-check">
                            <button type="submit" class="btn btn-login float-right">Ingresar</button>
                        </div>
                    </form>
                    <div class="copy-text"><i>GRUPO ALVARADO</i> </div>
                    <div class="copy-text1"><i>Version 2.0.0</i> </div>
                </div>
                <div class="col-md-8 banner-sec">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                               <img class="d-block w-100" src="public/images/img1.png" alt="First slide">
                            </div>
                            <div class="carousel-item">
                               <img class="d-block w-100" src="public/images/img2.jpeg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                               <img class="d-block w-100" src="public/images/img3.jpeg" alt="Third slide">
                            </div>
                        </div>
                              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                    </div>
                </div>
            </div>
            </div>
        </section>';
        
        \dti_core::set('css', '<link href="public/css/login/estiloLogin.css" rel="stylesheet" type="text/css"/>');
        /*
        $contatenar = $this->layout->renderizar(array(
            'section'=>array(
                'manual'=>$manual,
            )
        ));*/
        
        $this->view('index', array(
            'titulo'=>'Login',
            'layout'=>$manual
        ));
    }
}
