<?php

namespace Models;

class Dti_websiteModel extends \core\ModeloBase {
    private $table;
        
    public function __construct($adapter) {
        $this->table = "dti_website";
        parent::__construct($this->table, $adapter);
    }
    
    public function getWebsite(){
        $query="SELECT dw.id,dw.nombre,dw.description,dw.telefono,dw.keywords,dw.website_url,dw.wscobranza, dw.logo,dw.icon,dw.info_email,dw.copyright,dw.smtp_hostname, dw.smtp_port,dw.smtp_username,dw.clase_clientes,dw.smtp_password,dw.iva, dwl.lenguaje, dwl.abreviatura ,(SELECT dwt.template FROM dti_website_template dwt WHERE dw.template_portal = dwt.id) as template_portal ,(SELECT dwt.id FROM dti_website_template dwt WHERE dw.template_portal = dwt.id) as portal_id ,(SELECT dwt2.template FROM dti_website_template dwt2 WHERE dw.template_core = dwt2.id) as template_core ,(SELECT dwt2.id FROM dti_website_template dwt2 WHERE dw.template_core = dwt2.id) as core_id FROM dti_website dw INNER JOIN dti_website_language dwl ON dw.languageid = dwl.id ";
        $dti_user=$this->ejecutarSql($query);
        return $dti_user;
    }
}