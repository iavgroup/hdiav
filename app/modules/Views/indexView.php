<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <base href="<?php echo APP_URL ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/cyborg/bootstrap.min.css" rel="stylesheet" integrity="sha384-5KpNAytmiDxBanrkBv7mxkrUV1uggYCd+nLTGVig8i0ubJLVBFWQKXnGK5nR4kJF" crossorigin="anonymous">
        <script src="<?php echo PATH_JS ?>jquery.min.js" type="text/javascript"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js" integrity="sha384-lZmvU/TzxoIQIOD9yQDEpvxp6wEU32Fy0ckUgOH4EIlMOCdR823rg4+3gWRwnX1M" crossorigin="anonymous"></script>
        <link href="<?php echo PATH_CSS ?>fonts/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo PATH_CSS ?>fonts/line-awesome.css" rel="stylesheet" type="text/css"/>
        <!-- Css Declarados -->
        <?php if(isset($css)) {echo $css;} ?>
        <!-- Js Declarados -->
        <?php if(isset($js)) {echo $js;} ?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id='_AJAX_ERROR_' class="alert_top"></div>
        <?php if(isset($layout)) {echo $layout;} ?>
        <?php if(isset($modal)) {echo $modal;} ?>
        <?php if(isset($script)) {echo $script;} ?>
    </body>
</html>