<?php

namespace config;

//Usuario a Ejecutar
$confuser = 'gabriel'; //Digitar el nombre de application.ini
$conf = parse_ini_file('application.ini', TRUE);

/*
 * Titulo: Variables Globales
 * Author: Gabriel Reyes
 * Fecha: 27/04/2017
 * Version: 1.0.2
 */

/* VARIABLES BASE DE DATOS */
define("BD_DRIVER",$conf[$confuser]['driver']);
define("BD_HOST",$conf[$confuser]['host']);
define("BD_USER",$conf[$confuser]['username']);
define("BD_PASS",$conf[$confuser]['password']);
define("BD_DATABASE",$conf[$confuser]['database']);
define("DB_CHARSET",$conf[$confuser]['charset']);

/* VARIABLES GLOBALES */
define("CONTROLADOR_DEFECTO",$conf[$confuser]['controller']);
define("ACCION_DEFECTO",$conf[$confuser]['accion']);
define('APP_URL',$conf[$confuser]['base_dir']);

/* VARIABLES GLOBALES PATH */
define('PATH_IMAGES','public/images/');
define('PATH_MODULOS','app/modules/');
define('PATH_CSS','public/css/');
define('PATH_JS','public/js/');

/* PERMITIR VARIABLES DE SESSION */
session_start();

/* INICIALIZAR LIBRERIAS */
define('PRUEBA',$conf[$confuser]['prueba']);

//Cargamos Nucle DtiCore
require_once 'lib/dticore/autoloader.php';