<?php

namespace config;
/*
 * Titulo: Funciones Globales
 * Author: Gabriel Reyes
 * Fecha: 20/04/2017
 * Version: 1.0.1
 * --------------------------------
 * Cambios: Nomenclatura de funciones, funcion mensajes
 * Version: 1.0.2
 * --------------------------------
 * Cambios: Nuevas Funciones: * Llamadas a WebService
 *                            * Crear Mensajes de Confirmación
 * Version: 1.0.3
 */

class globalFunctions {
    /*
     * Variables Globales
     */
    private static $tipo;
    
    /**
     * Funcion: Insertar dentro de los id el codigo necesario
     * 
     * @param array $render estructura del template
     * @param array $section_contents_tpl contenidos a actualizarse
     * @return string html para mostrar en la vista
     */
    public static function renderizarTotal($render='',$section_contents_tpl=''){
        if ($render) {
            foreach ($section_contents_tpl as $key => $section_content)
            {
                $view = '';
                if($section_content)
                {
                    //last find of section_name
                    $pos_occ = strpos($render, $key);
                    if($pos_occ)
                    {
                        //next part of string after are_name as id
                        $render_part = substr($render, $pos_occ);
                        //then find >
                        $pos_grt = strpos($render_part, '>');
                        //next part of string after >
                        $render_part = substr($render_part, $pos_grt);
                        //then find </
                        $pos_lss = strpos($render_part, '</');
                        //next part of string after </
                        $render_part = substr($render_part, $pos_lss);

                        $view = substr_replace($render, $section_content, ($pos_occ+$pos_grt+1));
                        $view.= $render_part;
                        $render = $view;
                    }
                }
            }
        }
        return $render;
    }
    
    /**
     * Funcion: Carga los controladores Creados
     * 
     * @param string $controller nombre del controlador
     * @return \config\controlador
     */
    public static function cargarControlador($controller=''){
        $controlador= "Controllers\\".ucwords($controller)."Controllers";
        $strFileController=PATH_MODULOS.'default/'.str_replace("\\", "/", $controlador).'.php';
        self::$tipo = 'default';
        if (!is_file($strFileController)) {
            $controlador= "Controllers\\".ucwords($controller)."Controllers";
            $strFileController=PATH_MODULOS.'dticore/'.str_replace("\\", "/", $controlador).'.php';
            self::$tipo = 'dticore';
            if (!is_file($strFileController)) {
                $controlador= "Controllers\\".ucwords(CONTROLADOR_DEFECTO)."Controllers";
                $strFileController=PATH_MODULOS.'default/'.str_replace("\\", "/", $controlador).'Controllers.php';
                self::$tipo = 'default';
            }
        }
        
        $controllerObj=new $controlador(self::$tipo);
        return $controllerObj;
    }
    
    /**
     * Funcion: Carga las acciones de cada controlador
     * 
     * @param string $controllerObj Controlador
     * @param string $action accion
     */
    public static function cargarAccion($controllerObj='',$action=''){
        $accion=$action;
        $controllerObj->$accion();
    }

    /**
     * Funcion: Une la accion con el controlador
     * 
     * @param string $controllerObj Controlador
     */
    public static function lanzarAccion($controllerObj=''){
        if (isset($_GET["action"]) && method_exists($controllerObj, $_GET["action"])) {
            globalFunctions::cargarAccion($controllerObj,$_GET["action"]);
        }else{
            if (self::$tipo == 'default') {
                globalFunctions::cargarAccion($controllerObj,ACCION_DEFECTO);
            }else{
                globalFunctions::cargarAccion($controllerObj,"index");
            }
        }
    }
    
    
}
