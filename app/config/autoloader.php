<?php

//Registra las clases necesarias para la funcion de cada Controlador
spl_autoload_register(function($clase){
    //Agrega Configuracion
    $ruta = "app/".str_replace("\\", "/", $clase).".php";
    if (is_readable($ruta)) {
        require_once $ruta;
    }else{
        //Agrega Controladores
        $ruta = "app/modules/".str_replace("\\", "/", $clase).".php";
        if (is_readable($ruta)) {
            require_once $ruta;
        } else {
            //Agrega elementos del core
            $ruta = APP_URL."app/core/".str_replace("\\", "/", $clase).".php";
            if (is_readable($ruta)) {
                require_once $ruta;
            }else{
                //Agrega Configuraciones
                $ruta = "app/config/".str_replace("\\", "/", $clase).".php";
                if (is_readable($ruta)) {
                    require_once $ruta;
                }
            }
        }
    }
});
