<?php
namespace core;

class ModeloBase extends EntidadBase {
    private $table;

    public function __construct($table,$adapter) {
        $this->table=(string) $table;
        parent::__construct($table,$adapter);
    }

    public function ejecutarSql($sql){
        $query=$this->db()->query($sql);
        if($query==true){
            if($query->num_rows>1){
                while($row = $query->fetch_assoc()) {
                   $resultSet[]=$row;
                }
            }elseif($query->num_rows==1){
                if($row = $query->fetch_assoc()) {
                    $resultSet=$row;
                }
            }else{
                $resultSet=true;
            }
        }else{
            $resultSet=false;
        }
        return $resultSet;
    }

    public function ejecutarSqlBool($sql){
        $query=$this->db()->query($sql);
        if($query->num_rows>1){
            $resultSet = true;
        }else{
            if ($query->num_rows==1) {
                $resultSet = true;
            }else{
                $resultSet = false;
            }
        }
        return $resultSet;
    }
}