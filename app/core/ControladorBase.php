<?php
namespace core;

class ControladorBase{
    
    private $tipo;

    public function __construct($tipo) {
        $this->tipo = $tipo;
    }
    //Plugins y funcionalidades
    public function view($vista,$datos){
        foreach ($datos as $id_assoc => $valor){
            ${$id_assoc}=$valor;
        }
        
        //Agregamos el autor
        $autor = $this->website["copyright"];
        //Agregamos la descripcion
        $descripcion = $this->website["description"];
        //Agregamos el favicon
        $favicon = $this->website["icon"];
        //Manejo de Titulos
        if (isset($titulo)) { $titulo = $this->website["nombre"] ." - " .  $titulo; }
        else { $titulo = $this->website["nombre"]; }
        //Manejo de css
        if (isset($css)) { $css = $css . \dti_core::get()["css"]; }
        else{
            if (isset(\dti_core::get()["css"])) { $css = \dti_core::get()["css"]; }
        }
        //Manejo de js
        if (isset($js)) { $js = $js . \dti_core::get()["js"]; }
        else{
            if (isset(\dti_core::get()["js"])) { $js = \dti_core::get()["js"]; }
        }
        //Manejo de script
        if (isset($script)) {
            if (isset(\dti_core::get()["script"])) { $script = $script . \dti_core::get()["script"]; }
            else { $script = $script; }
        }
        else{
            if (isset(\dti_core::get()["script"])) { $script = \dti_core::get()["script"]; }
        }
        //Manejo de modal
        if (isset($modal)) { 
            if (isset(\dti_core::get()["script"])) { $modal = $modal . \dti_core::get()["modal"]; }
            else { $modal = $modal; }
        }
        else{
            if (isset(\dti_core::get()["modal"])) { $modal = \dti_core::get()["modal"]; }
        }
        //Activamos la vista
        require_once PATH_MODULOS.'/Views/'.$vista.'View.php';
    }

    public function redirect($controlador=CONTROLADOR_DEFECTO,$accion=ACCION_DEFECTO){
        header("Location:".APP_URL."".$controlador."/".$accion);
    }
}