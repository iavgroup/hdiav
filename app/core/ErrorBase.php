<?php
namespace core;

// Creamos una exception especial
class PHPException extends \Exception { }
 
// Deberíamos hacer que nuestra clase de BBDD en vez de lanzar Exceptions genéricas lance DBExceptions...
class DBException extends \Exception { }
 
// Separamos las excepciones de código
set_error_handler('errorDti');
function errorDti($code, $error, $file = NULL, $line = NULL) {
    throw new PHPException($error . ' encontrado en '. $file.', línea '.$line);
}