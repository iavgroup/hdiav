<?php
namespace core;

class Conectar{
    private $driver;
    private $host, $user, $pass, $database,$charset;
   
    public function __construct() {
        $this->driver=BD_DRIVER;
        $this->host=BD_HOST;
        $this->user=BD_USER;
        $this->pass=BD_PASS;
        $this->database=BD_DATABASE;
        $this->charset=DB_CHARSET;
    }

    public function conexion(){
        $con=new \mysqli($this->host, $this->user, $this->pass, $this->database);
        $con->query("SET NAMES '".$this->charset."'");
        return $con;
    }
}