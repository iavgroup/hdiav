<?php
namespace core;

class EntidadBase {
    /*
     * DECLARACION DE VARIABLES
     */
    private $table;
    private $db;
    private $conectar;
 
    /**
     * Constructor
     * @param string $table nombre de la tabla
     * @param string $adapter cadena de conexión
     */
    public function __construct($table, $adapter) {
        $this->table=(string) $table;
        $this->conectar = null;
        $this->db = $adapter;
    }
     
    /**
     * Variable de conexion
     * @return string cadena de conexion
     */
    public function db(){
        return $this->db;
    }
     
    private function dtresult($query){
        if ($query->num_rows>1) {
            while ($row = $query->fetch_assoc()) {
                $resultSet[]=$row;
            }            
        }else{
            if ($query->num_rows==1) {
                if($row = $query->fetch_assoc()) {
                    $resultSet=$row;
                }
            }else{
                $resultSet[]=null;
            }
        }
        return $resultSet;
    }
    
    //#####################################################
    //         MANEJADOR DE VALIDADORES BOOLEAN
    //#####################################################
    
    public function checkID($id){
        $sql = "SELECT * FROM $this->table WHERE ID= $id";
        $query=$this->db->query($sql) or die(mysqli_error($this->db));        
        return $this->dtresultBool($query);
    }

    //#####################################################
    //              MANEJADOR DE SELECT 
    //#####################################################
    
    public function getQuery($sql){
        $query=$this->db->query($sql) or die('<div class="alert alert-dismissible alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <h4>ERROR!</h4>
                                                    <p><strong>' . mysqli_error($this->db) . '</strong></p>
                                                  </div>');
        return $this->dtresult($query);
    }
    
    public function getAll($column='id'){
        $sql = "SELECT * FROM $this->table ORDER BY $column DESC";
        $query=$this->db->query($sql) or die(mysqli_error($this->db));      
        return $this->dtresult($query);
    }
    
    public function getAllActivo(){
        $sql = "SELECT * FROM $this->table WHERE activo = 1 ORDER BY id DESC";
        $query=$this->db->query($sql) or die(mysqli_error($this->db)); 
        return $this->dtresult($query);
    }
    
    public function getCount($column='id'){
        $sql = "SELECT count($column) as numrows FROM $this->table ";
        $query=$this->db->query($sql) or die(mysqli_error($this->db)); 
        return $this->dtresult($query);
    }
    
    public function getById($id){
        $sql = "SELECT * FROM $this->table WHERE id=$id";
        $query=$this->db->query($sql) or die(mysqli_error($this->db)); 
        return $this->dtresult($query);
    }
    
    //#####################################################
    //              MANEJADOR DE DELETE
    //#####################################################    

    public function deleteById($id){
        $sql = "DELETE FROM $this->table WHERE id=$id";
        $query=$this->db->query($sql) or die(mysqli_error($this->db)); 
        return $query;
    }
    
    //#####################################################
    //              MANEJADOR DE UPDATE
    //#####################################################
    
    /**
     * Actualiza una columna a la vez
     * @param int $id Id de la tabla a actualizar
     * @param string $column Nombre de la columna a actualizar
     * @param string $value Valor que se va actualizar
     * @return string
     */
    public function updateBy($id,$column,$value){
        $sql = "UPDATE $this->table SET $column='$value' WHERE id = $id";
        $query=$this->db->query($sql) or die(mysqli_error($this->db)); 
        return $query;
    }
}