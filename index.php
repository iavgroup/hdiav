<?php

require_once 'app/config/autoloader.php';
require_once 'app/config/global.php';

try {
    if (isset($_GET["controller"])) {
        $controllerObj= config\globalFunctions::cargarControlador($_GET["controller"]);
    }else{
        $controllerObj= config\globalFunctions::cargarControlador(CONTROLADOR_DEFECTO);
    }
    config\globalFunctions::lanzarAccion($controllerObj);
} catch (PDOException $ex){
    echo "PDOException".$ex->getMessage();
} catch(PHPException $ex) {
    echo "PHPException".$ex->getMessage();
} catch(DBException $ex) {
    echo "DBException".$ex->getMessage();
} catch(core\Exception $ex) {
    echo "Exception".$ex->getMessage().PHP_EOL;
}

